!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'fhs 4/19/2023 17:54:19'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de número inválido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 18:13:39'!
dividiraALaFraccion: aDividendFraccion
	"Reminder: Estamos haciendo fraccion dividido entero"
	^Fraccion with: aDividendFraccion numerator over: aDividendFraccion denominator * self! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 18:04:08'!
dividiraAlEntero: aDividendEntero
	^Fraccion with: aDividendEntero over: self
	! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/20/2023 10:55:49'!
dividiraDeFormaEnteraAlEntero: aDividendEntero
	^self class with: aDividendEntero integerValue // value
	
	! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:23:12'!
multiplicateConUnEntero: aMultiplierEntero
	"Aca si bien estamos haciendo el b*a que recibimos, no hay problema por conmutatividad"
	^self class with: value * aMultiplierEntero integerValue! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:30:40'!
multiplicateConUnaFraccion: aMultiplierFraccion
	"No hay problema de conmutatividad por ser *"
	^Fraccion with: self * aMultiplierFraccion numerator over: aMultiplierFraccion denominator
	! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:43:26'!
restateAUnEntero: aSubtrahendEntero
	"Aca debemos tener cuidado con que el orden si importa. Teniamos a - b y al metodo le llega que aSubtrahendEntero es el a"
	^self class with:  aSubtrahendEntero integerValue - value
	
! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:47:33'!
restateAUnaFraccion: aSubtrahendFraccion
	
	^Fraccion with: (aSubtrahendFraccion numerator - (self * aSubtrahendFraccion denominator)) over: aSubtrahendFraccion denominator
	
! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:53:10'!
sumateConUnEntero: anAdderEntero
	^self class with: (value + anAdderEntero integerValue)
	! !

!Entero methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:36:20'!
sumateConUnaFraccion: anAdderFraccion
	^Fraccion with: (self * anAdderFraccion denominator + anAdderFraccion numerator) over: anAdderFraccion denominator
	! !


!Entero methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 17:23:46'!
* aMultiplier 
	"Aca me estan pasando a * b. Al hacer lo de abajo hacemos b ... a. Hay que preservar el orden luego" 
	^aMultiplier multiplicateConUnEntero: self
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 17:54:52'!
+ anAdder 
	" Queda comentado para tenerlo de repaso
	(anAdder isKindOf: self class) ifTrue: [^self class with: value + anAdder integerValue.].
	(anAdder isKindOf: Fraccion) ifTrue: [^Fraccion with: (self * anAdder denominator + anAdder numerator) over: anAdder denominator].
	self error: 'Invalid number type'
	"
	^anAdder sumateConUnEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 17:56:22'!
- aSubtrahend 
	" Queda comentado para tenerlo de repaso
	(aSubtrahend isKindOf: self class) ifTrue: [^self class with: value - aSubtrahend integerValue.].
	(aSubtrahend isKindOf: Fraccion) ifTrue: [^Fraccion with: (self * aSubtrahend denominator - aSubtrahend numerator) over: aSubtrahend denominator].
	self error: 'Invalid number type'
	"
	^aSubtrahend restateAUnEntero: self
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 18:15:50'!
/ aDivisor 
	"Estamos haciendo a / b. Luego pasamos b mensaje a"
	"
	(aDivisor isKindOf: self class) ifTrue: [^Fraccion with: self over: aDivisor].
	(aDivisor isKindOf: Fraccion) ifTrue: [^Fraccion with: self * aDivisor denominator over: aDivisor numerator].
	self error: 'Invalid number type'
	"
	^aDivisor dividiraAlEntero: self
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'fhs 4/20/2023 15:54:54'!
// aDivisor 
	" Queda comentado para tenerlo de repaso
	(aDivisor isKindOf: Entero) ifTrue: [^self class with: value // aDivisor integerValue].
	self error: 'Invalid number type'"
	^aDivisor dividiraDeFormaEnteraAlEntero: self.
	
	
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'fhs 4/20/2023 15:57:00'!
fibonacci
	self subclassResponsibility		
	
	" Esta es una version que aprovecha la recursion. Construccion Bottom-Up.
	Funciona al separar Entero en EnterosPositivosYCero y EnterosNegativos. 
	Lo dejamos comentado puesto que fue una idea, pero decidimos seguir los pasos dados por la catedra.
	En EnterosNegativos iria el mensaje de error, y esto iria en EnterosPositivosYCero:
	| res f1 f2 |
	res := Entero with: 1.
	f1 := Entero with: 1.
	f2 := Entero with: 1.
	2 to: (self integerValue) do: [ :iter | 
		res := f1 + f2.
		f2 := f1.
		f1 := res.
	].
	^res
	"

! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:00'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (value gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'fhs 4/20/2023 10:48:34'!
= anObject

	^(anObject isKindOf: Entero) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 20:09'!
initalizeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:05:56'!
isNegative
	self subclassResponsibility		
! !

!Entero methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:06:54'!
isOne
	self subclassResponsibility ! !

!Entero methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:07:38'!
isZero
	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no está definido aquí para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'fhs 4/20/2023 16:03:39'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	(aValue < 0) ifTrue: [^EnterosNegativos new initalizeWith: aValue].
	(aValue = 0) ifTrue: [^EnteroCero new initalizeWith: aValue].
	(aValue = 1) ifTrue: [^EnteroUno new initalizeWith: aValue].
	^EnterosMayoresAUno new initalizeWith: aValue.
	
	"De la forma alternativa mencionada en Entero - fibonacci se puede lograr hacerlo con 2 ifs menos."! !


!classDefinition: #EnteroCero category: 'Numero-Exercise'!
Entero subclass: #EnteroCero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroCero methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:21:00'!
chequeoSiDividendoEsCeroConDivisor: aDivisor
	"Soy un dividendo 0... 0 / n = 0"
	^self! !

!EnteroCero methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 11:47:50'!
chequeoSiDivisorEsCero
	"Soy efectivamente un divisor 0. Debo elevar el error"
	 ^self error: Numero canNotDivideByZeroErrorDescription! !


!EnteroCero methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:05:15'!
isNegative
	^false! !

!EnteroCero methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:06:14'!
isOne
	^false! !

!EnteroCero methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:07:11'!
isZero
	^true! !


!EnteroCero methodsFor: 'arithmetic operations' stamp: 'fhs 4/20/2023 10:43:53'!
fibonacci
	^Entero with: 1.! !


!classDefinition: #EnteroUno category: 'Numero-Exercise'!
Entero subclass: #EnteroUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroUno methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:05:23'!
isNegative
	^false! !

!EnteroUno methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:06:22'!
isOne
	^true! !

!EnteroUno methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:07:19'!
isZero
	^false! !


!EnteroUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:37:11'!
chequeoSiDenominatorEsUnoConNumerator: aNumerator 
	"Si estoy aca mi denominator es 1"
	^aNumerator! !

!EnteroUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:22:31'!
chequeoSiDividendoEsCeroConDivisor: aDivisor
	"Si estoy aca es puesto que ni dividend ni divisor son cero"
	^aDivisor chequeoSiDivisorEsNegativoConDividendo: self
! !

!EnteroUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 11:47:50'!
chequeoSiDivisorEsCero
	"No hago nada ya que no debo elevar un error"! !

!EnteroUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:38:05'!
chequeoSiDivisorEsNegativoConDividendo: aDividend
	"Si estoy aca es porque mi divisor NO es negativo."
	| greatestCommonDivisor numerator denominator |
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: self. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.

	^denominator chequeoSiDenominatorEsUnoConNumerator: numerator.! !


!EnteroUno methodsFor: 'arithmetic operations' stamp: 'fhs 4/20/2023 10:43:39'!
fibonacci
	^Entero with: 1.! !


!classDefinition: #EnterosMayoresAUno category: 'Numero-Exercise'!
Entero subclass: #EnterosMayoresAUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnterosMayoresAUno methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:05:29'!
isNegative
	^false! !

!EnterosMayoresAUno methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:06:30'!
isOne
	^false! !

!EnterosMayoresAUno methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:07:25'!
isZero
	^false! !


!EnterosMayoresAUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:36:57'!
chequeoSiDenominatorEsUnoConNumerator: aNumerator 
	"Si estoy aca es porque mi denominator es mayor a uno"
	^Fraccion new initializeWith: aNumerator over: self! !

!EnterosMayoresAUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:22:48'!
chequeoSiDividendoEsCeroConDivisor: aDivisor
	"Si estoy aca es puesto que ni dividend ni divisor son cero"
	^aDivisor chequeoSiDivisorEsNegativoConDividendo: self! !

!EnterosMayoresAUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 11:47:50'!
chequeoSiDivisorEsCero
	"No hago nada ya que no debo elevar un error"! !

!EnterosMayoresAUno methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:38:13'!
chequeoSiDivisorEsNegativoConDividendo: aDividend
	"Si estoy aca es porque mi divisor NO es negativo."
	| greatestCommonDivisor numerator denominator |
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: self. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.
	
	^denominator chequeoSiDenominatorEsUnoConNumerator: numerator.! !


!EnterosMayoresAUno methodsFor: 'arithmetic operations' stamp: 'fhs 4/20/2023 15:55:56'!
fibonacci
	| one two |
	one := Entero with: 1.
	two := Entero with: 2.
	
	^ (self - one) fibonacci + (self - two) fibonacci
	! !


!classDefinition: #EnterosNegativos category: 'Numero-Exercise'!
Entero subclass: #EnterosNegativos
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnterosNegativos methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:05:34'!
isNegative
	^true! !

!EnterosNegativos methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:06:38'!
isOne
	^false! !

!EnterosNegativos methodsFor: 'testing' stamp: 'fhs 4/20/2023 11:07:29'!
isZero
	^false! !


!EnterosNegativos methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:22:53'!
chequeoSiDividendoEsCeroConDivisor: aDivisor
	"Si estoy aca es puesto que ni dividend ni divisor son cero"
	^aDivisor chequeoSiDivisorEsNegativoConDividendo: self! !

!EnterosNegativos methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 11:47:51'!
chequeoSiDivisorEsCero
	"No hago nada ya que no debo elevar un error"! !

!EnterosNegativos methodsFor: 'instance creation for fraccion' stamp: 'fhs 4/20/2023 15:22:31'!
chequeoSiDivisorEsNegativoConDividendo: aDividend
	"Si estoy aca es porque mi divisor es negativo."
	^aDividend negated / self negated! !


!EnterosNegativos methodsFor: 'arithmetic operations' stamp: 'fhs 4/20/2023 10:32:08'!
fibonacci
	self error: Entero negativeFibonacciErrorDescription! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 17:55:06'!
* aMultiplier 
	" Queda comentado para tenerlo de repaso
	(aMultiplier isKindOf: Entero) ifTrue: [^self class with: numerator * aMultiplier  over: denominator ].
	(aMultiplier isKindOf: Fraccion) ifTrue: [^(numerator * aMultiplier numerator) / (denominator * aMultiplier denominator)].
	self error: 'Invalid number type'
	"
	^aMultiplier multiplicateConUnaFraccion: self
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 17:55:11'!
+ anAdder 
	"
	| newNumerator newDenominator |
	(anAdder isKindOf: Entero) ifTrue: [^Fraccion with: anAdder * denominator + numerator over: denominator].
	(anAdder isKindOf: self class) ifTrue: [
	newNumerator := (numerator * anAdder denominator) + (denominator * anAdder numerator).
	newDenominator := denominator * anAdder denominator.
	^newNumerator / newDenominator ].
	self error: 'Invalid number type'
	"
	^anAdder sumateConUnaFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 17:56:56'!
- aSubtrahend 
	"
	| newNumerator newDenominator |
	(aSubtrahend isKindOf: Entero) ifTrue: [^Fraccion with: numerator - (aSubtrahend * denominator) over: denominator].
	(aSubtrahend isKindOf: self class) ifTrue: [
	newNumerator := (numerator * aSubtrahend denominator) - (aSubtrahend numerator * denominator).
	newDenominator := denominator * aSubtrahend denominator.
	^newNumerator / newDenominator ].
	self error: 'Invalid number type'
	"
	^aSubtrahend restateAUnaFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'fhs 4/19/2023 18:16:18'!
/ aDivisor 
	"(aDivisor isKindOf: Entero) ifTrue: [^Fraccion with:numerator over: denominator * aDivisor].
	(aDivisor isKindOf: self class) ifTrue: [^(numerator * aDivisor denominator) / (denominator * aDivisor numerator)].
	self error: 'Invalid number type'
	"
	^aDivisor dividiraALaFraccion: self
! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 18:15:20'!
dividiraALaFraccion: aDividendFraccion
	"Reminder: Estamos diviendo una fraccion aDividendFraccion por otra"
	^(aDividendFraccion numerator * denominator) / (aDividendFraccion denominator * numerator)! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 18:09:01'!
dividiraAlEntero: aDividendEntero
	"Reminder: Estamos queriendo dividir un entero por una fraccion"
	^Fraccion with: aDividendEntero * denominator over: numerator
! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:28:11'!
multiplicateConUnEntero: aMultiplierEntero
	
	^self class with: numerator * aMultiplierEntero over: denominator
! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:31:31'!
multiplicateConUnaFraccion: aMultiplierFraccion
	"No hay problema de conmutatividad por ser *"
	^(numerator * aMultiplierFraccion numerator) / (denominator * aMultiplierFraccion denominator)! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:45:02'!
restateAUnEntero: aSubtrahendEntero
	"Teniamos a - b y a este metodo le llega b restateAUnEntero: a"
	^Fraccion with:  (aSubtrahendEntero * denominator) - numerator over: denominator! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:48:35'!
restateAUnaFraccion: aSubtrahendFraccion
	| newNumerator newDenominator |
	
	newNumerator := (aSubtrahendFraccion numerator * denominator) - (numerator * aSubtrahendFraccion denominator).
	newDenominator := denominator * aSubtrahendFraccion denominator.
	
	^newNumerator / newDenominator! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:35:27'!
sumateConUnEntero: anAdderEntero
	
	^Fraccion with: anAdderEntero * denominator + numerator over: denominator
	! !

!Fraccion methodsFor: 'arithmetic auxiliar' stamp: 'fhs 4/19/2023 17:37:18'!
sumateConUnaFraccion: anAdderFraccion

	| newDenominator newNumerator |
	newNumerator := (numerator * anAdderFraccion denominator) + (denominator * anAdderFraccion numerator).
	newDenominator := denominator * anAdderFraccion denominator.
	
	^newNumerator / newDenominator! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'fhs 4/20/2023 16:07:32'!
with: aDividend over: aDivisor
	aDivisor chequeoSiDivisorEsCero. 
	^aDividend chequeoSiDividendoEsCeroConDivisor: aDivisor.
	"
	Para resolver este ejercicio buscamos ir delegandole el control a cada parte.
	Comenzamos con el metodo: chequeoSiDivisorEsCero el cual tira error si lo recibe EnteroCero
		Si lo recibe cualquier otro no hace nada.
	Luego: chequeoSiDividendoEsCeroConDivisor el cual devuelve 0 si lo recibe EnteroCero
		Si lo recibe cualquier otro manda el mensaje de abajo:
	chequeoSiDivisorEsNegativoConDividendo el cual niega ambas parte de la division
		Si lo recibe cualquier otro:
	Busca el GCD, calcula el numerator y denominator y llama a chequeoSiDenominatorEsUnoConNumerator
	el cual devuelve numerator si lo recibe el 1, caso contrario devuelve la fraccion.
	
	Al utilizar un constante pasaje de mensajes, logramos evitar el uso del if
	"

	! !
