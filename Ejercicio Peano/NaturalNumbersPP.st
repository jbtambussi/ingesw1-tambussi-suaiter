!classDefinition: #I category: 'NaturalNumbersPP'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbersPP'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'NaturalNumbersPP'!
I class
	instanceVariableNames: 'sig prev'!

!I class methodsFor: 'as yet unclassified' stamp: 'FHS 3/30/2023 15:03:07'!
* aNaturalNumber
	^aNaturalNumber! !

!I class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:15:16'!
+ aNaturalNumber
	^aNaturalNumber next.! !

!I class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:47:35'!
/ aDivisor
    ^aDivisor timesDivideTo: self! !

!I class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:47:58'!
divideWithDividendGreaterEqualThanDivisor: aDividend and: aDivisor
    "Caso 'Equal' del GreaterEqual"
    ^self! !

!I class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:18:10'!
next
	sig ifNil: [
		sig:= self cloneNamed: self name, 'I'.
		sig setPrev: self.
		].
	^sig! !

!I class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 20:53:58'!
prev
^prev! !

!I class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 20:53:34'!
setPrev: valorPrevio
	prev := valorPrevio ! !

!I class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:47:50'!
timesDivideTo: aDividend
    ^aDividend.! !


!I class methodsFor: '--** private fileout/in **--' stamp: 'fd 4/2/2023 16:50:21'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	sig := II.
	prev := nil.! !


!classDefinition: #II category: 'NaturalNumbersPP'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbersPP'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'NaturalNumbersPP'!
II class
	instanceVariableNames: 'sig prev'!

!II class methodsFor: 'as yet unclassified' stamp: 'FHS 3/30/2023 15:04:34'!
* aNaturalNumber
^(self prev * aNaturalNumber) + aNaturalNumber! !

!II class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:26:46'!
+ aNaturalNumber
	^self prev + aNaturalNumber next.! !

!II class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:43:14'!
- aNaturalNumber
	|previo val|
	previo:=aNaturalNumber prev.
	previo ifNil: [^self prev].
	previo ifNotNil: [val := self prev - aNaturalNumber prev].
	^val
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:48:09'!
/ aDivisor
    ^aDivisor timesDivideTo: self! !

!II class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:48:52'!
canNotDivideByBiggerNumberErrorDescription
    ^ 'No se puede dividir'! !

!II class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:49:40'!
divideWithDividendGreaterEqualThanDivisor: aDividend and: aDivisor
"Caso 'Greater' del GreaterEqual"
    ^[(aDividend - aDivisor) / aDivisor + I
        ] on:Error do: [I]! !

!II class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:26:21'!
next
	sig ifNil: [
		sig:= self cloneNamed: self name, 'I'.
		sig setPrev: self.
		].
	^sig! !

!II class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:26:21'!
prev
^prev! !

!II class methodsFor: 'as yet unclassified' stamp: 'ff 3/27/2023 21:26:21'!
setPrev: valorPrevio
	prev := valorPrevio ! !

!II class methodsFor: 'as yet unclassified' stamp: 'fd 4/2/2023 16:49:13'!
timesDivideTo: aDividend

"No tenemos comparacion disponible para saber si divisor es mas grande que dividendo, habra que ver si falla"
"El + I es para salvar el caso de division entre iguales"

    ^ [(aDividend + I - self) divideWithDividendGreaterEqualThanDivisor:  aDividend and: self        ] 
        on: Error 
        do: [self error: self canNotDivideByBiggerNumberErrorDescription]! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'fd 4/2/2023 16:50:21'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	sig := AnObsoleteIII.
	prev := I.! !

I initializeAfterFileIn!
II initializeAfterFileIn!