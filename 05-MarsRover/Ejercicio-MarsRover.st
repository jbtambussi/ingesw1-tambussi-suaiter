!classDefinition: #MarsRoverTest category: 'Ejercicio-MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!MarsRoverTest methodsFor: 'assert' stamp: 'FHS 5/15/2023 11:29:43'!
assertMarsRover: aMarsRover isOn: aPosition facing: aCardinalDirection
	self assert: aPosition equals: aMarsRover position.
	self assert: aCardinalDirection equals: aMarsRover direction.
	"Segun lo visto en la Teorica, tener dos asserts siempre y cuando tengan sentido (evaluan la esencia del MarsRover) no estaría mal"! !


!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 15:39:09'!
test01MarsRoverComienzaEnUnPuntoInicialX1Y1ApuntandoAlNorte
	|marsRover|
	marsRover := MarsRover startOn: 1@1 facing: Norte new.
	self assertMarsRover: marsRover isOn: 1@1 facing: Norte new.! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 15:39:16'!
test02MarsRoverComienzaEnUnPuntoInicialX2Y5ApuntandoAlNorte
	|marsRover|
	marsRover := MarsRover startOn: 2@5 facing: Norte new.
	self assertMarsRover: marsRover isOn: 2@5 facing: Norte new! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 15:39:25'!
test03MarsRoverComienzaEnUnPuntoInicialX4Y4ApuntandoAlSur
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Sur new.
	self assertMarsRover: marsRover isOn: 4@4 facing: Sur new.! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 15:40:40'!
test04MarsRoverConListaDeComandosVacioNoHaceNada
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Sur new.
	marsRover readCommands: ''.
	self assertMarsRover: marsRover isOn: 4@4 facing: Sur new.! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 11:21:22'!
test05MarsRoverConUnComandoQueMueveAdelanteConDireccionNorte
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Norte new.
	marsRover readCommands: 'f'.
	self assertMarsRover: marsRover isOn: 4@5 facing: Norte new.! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 11:30:05'!
test06MarsRoverConUnComandoQueMueveAdelanteConDireccionSur
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Sur new.
	marsRover readCommands: 'f'.
	self assertMarsRover: marsRover isOn: 4@3 facing: Sur new.! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 11:21:56'!
test07MarsRoverConUnComandoQueMueveAtrasConDireccionNorte
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Norte new.
	marsRover readCommands: 'b'.
	self assertMarsRover: marsRover isOn: 4@3 facing: Norte new.
	"Siempre asumiendo que el norte es +1 a y"! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 11:22:16'!
test08MarsRoverConUnComandoQueRotaAIzquierda
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Norte new.
	marsRover readCommands: 'l'.
	self assertMarsRover: marsRover isOn: 4@4 facing: Oeste new.
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 11:22:30'!
test09MarsRoverConUnComandoQueRotaADerecha
	|marsRover|
	marsRover := MarsRover startOn: 4@4 facing: Norte new.
	marsRover readCommands: 'r'.
	self assertMarsRover: marsRover isOn: 4@4 facing: Este new.
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 15:40:12'!
test10MarsRoverEjecutaConVariosComandos
	|marsRover|
	marsRover := MarsRover startOn: 0@0 facing: Norte new.
	marsRover readCommands: 'ffrffb'.
	self assertMarsRover: marsRover isOn: 1@2 facing: Este new.! !

!MarsRoverTest methodsFor: 'tests' stamp: 'FHS 5/15/2023 11:23:40'!
test11MarsRoverEjecutaHastaEncontrarUnComandoDesconocido
	|marsRover|
	marsRover := MarsRover startOn: 0@0 facing: Norte new.
	
	self 
		should: [ 		marsRover readCommands: 'ffhff'.		] 
		raise: Error 
		withExceptionDo: [ :error | 
			self assert: error messageText equals: MarsRover notAnAvaliableCommandErrorDescription .
			self assertMarsRover: marsRover isOn: 0@2 facing: Norte new.]! !


!classDefinition: #MarsRover category: 'Ejercicio-MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'position direction commands'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!MarsRover methodsFor: 'movement' stamp: 'FHS 5/15/2023 11:14:20'!
moveOneBackward
	position := position - direction vectorPointingFoward.! !

!MarsRover methodsFor: 'movement' stamp: 'FHS 5/15/2023 11:14:41'!
moveOneFoward
	position := position + direction vectorPointingFoward.! !


!MarsRover methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:58:05'!
rotateLeft
	direction := direction rotate90DegreesLeft.! !

!MarsRover methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:58:43'!
rotateRight
	direction := direction rotate90DegreesRight.! !


!MarsRover methodsFor: 'initialization' stamp: 'FHS 5/15/2023 11:26:03'!
initializeWithPosition: aPosition andDirection: aDIrection 
	self setCommandsForRover.
	position := aPosition.
	direction := aDIrection.! !

!MarsRover methodsFor: 'initialization' stamp: 'FHS 5/15/2023 11:26:08'!
setCommandsForRover
	commands := Dictionary new.
	commands at: $f put: [self moveOneFoward].
	commands at: $b put: [self moveOneBackward].
	commands at: $l put: [self rotateLeft].
	commands at: $r put: [self rotateRight].
	
	"Creamos el diccionario de a poco puesto que con newFromPairs no contempla bien las closures"! !


!MarsRover methodsFor: 'testing' stamp: 'FHS 5/12/2023 14:47:19'!
direction
	^direction.! !

!MarsRover methodsFor: 'testing' stamp: 'FHS 5/12/2023 14:36:53'!
position
	^position! !


!MarsRover methodsFor: 'reading' stamp: 'FHS 5/15/2023 11:26:58'!
readCommands: stringOfCommands
	stringOfCommands do: [:char | 
		(commands at: char ifAbsent: [self error: self class notAnAvaliableCommandErrorDescription]) value.
	]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'Ejercicio-MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'error description' stamp: 'FHS 5/15/2023 10:42:30'!
notAnAvaliableCommandErrorDescription
	^'Se ingreso un comando invalido'! !


!MarsRover class methodsFor: 'initialization' stamp: 'FHS 5/12/2023 14:49:23'!
startOn: aPosition facing: aCardinalDirection

	^self new initializeWithPosition: aPosition andDirection: aCardinalDirection.! !


!classDefinition: #PuntoCardinal category: 'Ejercicio-MarsRover'!
Object subclass: #PuntoCardinal
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!PuntoCardinal methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:54:31'!
rotate90DegreesLeft
	self subclassResponsibility ! !

!PuntoCardinal methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:54:38'!
rotate90DegreesRight
	self subclassResponsibility ! !


!PuntoCardinal methodsFor: 'vector direction' stamp: 'FHS 5/15/2023 11:31:25'!
vectorPointingFoward
	self subclassResponsibility 
	
	"Interpretamos que en el plano:
	Y positivo es el Norte
	Y negativo es el Sur
	X positivo es el Este
	X negativo es el Oeste	
	"! !


!PuntoCardinal methodsFor: 'equal' stamp: 'FHS 5/15/2023 11:07:58'!
= aCardinalPoint
	^self class = aCardinalPoint class.! !


!classDefinition: #Este category: 'Ejercicio-MarsRover'!
PuntoCardinal subclass: #Este
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!Este methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:55:41'!
rotate90DegreesLeft
	^Norte new.! !

!Este methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:55:53'!
rotate90DegreesRight
	^Sur new.! !


!Este methodsFor: 'vector direction' stamp: 'FHS 5/15/2023 11:13:38'!
vectorPointingFoward
	^(1@0)! !


!classDefinition: #Norte category: 'Ejercicio-MarsRover'!
PuntoCardinal subclass: #Norte
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!Norte methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:55:19'!
rotate90DegreesLeft
	^Oeste new.! !

!Norte methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:55:09'!
rotate90DegreesRight
	^Este new.! !


!Norte methodsFor: 'vector direction' stamp: 'FHS 5/15/2023 11:13:38'!
vectorPointingFoward
	^(0@1)! !


!classDefinition: #Oeste category: 'Ejercicio-MarsRover'!
PuntoCardinal subclass: #Oeste
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!Oeste methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:56:48'!
rotate90DegreesLeft
	^Sur new.! !

!Oeste methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:56:56'!
rotate90DegreesRight
	^Norte new.! !


!Oeste methodsFor: 'vector direction' stamp: 'FHS 5/15/2023 11:13:38'!
vectorPointingFoward
	^(-1@0)! !


!classDefinition: #Sur category: 'Ejercicio-MarsRover'!
PuntoCardinal subclass: #Sur
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ejercicio-MarsRover'!

!Sur methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:56:17'!
rotate90DegreesLeft
	^Este new.! !

!Sur methodsFor: 'rotation' stamp: 'FHS 5/15/2023 10:56:26'!
rotate90DegreesRight
	^Oeste new.! !


!Sur methodsFor: 'vector direction' stamp: 'FHS 5/15/2023 11:13:38'!
vectorPointingFoward
	^(0@-1)! !
