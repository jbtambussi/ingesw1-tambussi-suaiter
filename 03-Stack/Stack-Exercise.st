!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'fhs 4/22/2023 21:54:27'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'sfbp sfbpStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'setUp' stamp: 'fhs 4/24/2023 10:58:43'!
setUp
	sfbp := SentenceFinderByPrefix new.
	sfbpStack := OOStack new.! !


!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'fhs 4/24/2023 11:14:43'!
test01BusquedaEnStackVacioNoDevuelveNada
	|resultado|
	resultado := sfbp findOnStack: sfbpStack thePrefix: 'hola'.
	self assert: resultado isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'fhs 4/24/2023 11:35:26'!
test02BusquedaEnStackNoModificaAlMismo
	|val1 val2 val3|
	
	sfbpStack push: 'Carlos F'.
	sfbpStack push: 'Enrique'.
	sfbpStack push: 'Carlos E'.
	
	sfbp findOnStack: sfbpStack thePrefix: 'Carlos'.
	
	val1 := (sfbpStack pop).
	val2 := (sfbpStack pop).
	val3 := (sfbpStack pop).
	
	self assert: val1 = 'Carlos E'.
	self assert: val2 = 'Enrique'.
	self assert: val3 = 'Carlos F'.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'fhs 4/24/2023 11:16:40'!
test03BusquedaEncuentraElPrefijoValido
	sfbpStack push: 'hola caracola'.
	self assert: (sfbp findOnStack: sfbpStack thePrefix: 'hola') first = 'hola caracola'.
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'FHS 4/24/2023 15:37:42'!
test04prefijoDebeSerTexto
sfbpStack push: 'Hola'.
sfbpStack push: 'Chau'.
self should: [ sfbp findOnStack: sfbpStack thePrefix: 492 ]
		raise: Error 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixIsNotACharacterSequenceErrorDescription ]

! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'FHS 4/24/2023 15:18:19'!
test05prefijoNoPuedeSerVacio
sfbpStack push: 'Hola'.
sfbpStack push: 'Chau'.
self should: [ sfbp findOnStack: sfbpStack thePrefix: '' ]
	raise: Error 
	withExceptionDo: [ :anError |
		self assert: anError messageText = SentenceFinderByPrefix prefixEmptyErrorDescription ]
! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'FHS 4/24/2023 15:32:18'!
test06prefijoNoPuedeSerEspacios
sfbpStack push: 'Hola'.
sfbpStack push: 'Chau'.
self should: [ sfbp findOnStack: sfbpStack thePrefix: '  ' ]
	raise: Error 
	withExceptionDo: [ :anError |
		self assert: anError messageText = SentenceFinderByPrefix prefixSepatorErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'FHS 4/24/2023 15:20:10'!
test07prefijoNoPuedeTenerSeparadores
sfbpStack push: 'Hola'.
sfbpStack push: 'Chau'.
self should: [ sfbp findOnStack: sfbpStack thePrefix: 'a b' ]
	raise: Error 
	withExceptionDo: [ :anError |
		self assert: anError messageText = SentenceFinderByPrefix prefixSepatorErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'fhs 4/24/2023 11:29:37'!
test08BusquedaEsCaseSensitive
	|resultado|
	sfbpStack push: 'hola caracola'.
	sfbpStack push: 'hola'.
	sfbpStack push: 'holA'.
	sfbpStack push: 'hOla'.
	resultado := sfbp findOnStack: sfbpStack thePrefix: 'Hola'.
	self assert: resultado isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'fhs 4/24/2023 11:26:52'!
test09BusquedaNoEncuentraNingunElemento
	|resultado|
	sfbpStack push: 'Carlos F'.
	sfbpStack push: 'Enrique'.
	sfbpStack push: 'Carlos E'.
	sfbpStack push: 'Ariana'.
	resultado := sfbp findOnStack: sfbpStack thePrefix: 'Gabriel'.
	self assert: resultado isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'fhs 4/24/2023 11:28:30'!
test10BusquedaEncuentraMasDeUnElemento
	|resultado|
	sfbpStack push: 'Carlos F'.
	sfbpStack push: 'Enrique'.
	sfbpStack push: 'Carlos E'.
	sfbpStack push: 'Ariana'.
	resultado := sfbp findOnStack: sfbpStack thePrefix: 'Carlos'.
	self assert: ((resultado includes: 'Carlos F') and: [resultado includes: 'Carlos E']) .! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'stack tamaño'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'basic operations' stamp: 'fhs 4/22/2023 23:00:38'!
pop
	^(stack first) objectToBePoppedOnStack: self.
	! !

!OOStack methodsFor: 'basic operations' stamp: 'FHS 4/24/2023 15:27:00'!
push: somethingToAdd
	tamaño := tamaño + 1.
	^stack addFirst: (RegularObject with: somethingToAdd).
! !

!OOStack methodsFor: 'basic operations' stamp: 'fhs 4/22/2023 23:14:35'!
top
	^(stack first) objectToBeToppedOnStack: self.! !


!OOStack methodsFor: 'confirmation (double dispatch)' stamp: 'fhs 4/23/2023 23:20:24'!
procurementOfTopObjectConfirmation
	^stack first value. ! !

!OOStack methodsFor: 'confirmation (double dispatch)' stamp: 'FHS 4/24/2023 15:27:19'!
removalOfObjectConfirmation
	tamaño := tamaño - 1.
	^stack removeFirst value. ! !


!OOStack methodsFor: 'information' stamp: 'fhs 4/22/2023 11:46:47'!
isEmpty
	^tamaño = 0.! !

!OOStack methodsFor: 'information' stamp: 'fhs 4/22/2023 21:54:27'!
size
	^tamaño.! !


!OOStack methodsFor: 'initialization' stamp: 'fhs 4/23/2023 23:16:03'!
initialize
	stack := OrderedCollection new.
	stack addFirst: (FlagObject with: 'Flag').
	tamaño := 0.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #ObjectsOfOOStack category: 'Stack-Exercise'!
Object subclass: #ObjectsOfOOStack
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!ObjectsOfOOStack methodsFor: 'initialization' stamp: 'fhs 4/22/2023 23:19:39'!
initializeWith: valorAsignado
	value := valorAsignado.
	! !


!ObjectsOfOOStack methodsFor: 'subclass Resp' stamp: 'JBT 4/24/2023 16:53:12'!
objectToBePoppedOnStack: obj
	self subclassResponsibility ! !

!ObjectsOfOOStack methodsFor: 'subclass Resp' stamp: 'JBT 4/24/2023 16:53:10'!
objectToBeToppedOnStack: obj
	self subclassResponsibility ! !

!ObjectsOfOOStack methodsFor: 'subclass Resp' stamp: 'JBT 4/24/2023 16:53:20'!
value
	self subclassResponsibility ! !


!classDefinition: #FlagObject category: 'Stack-Exercise'!
ObjectsOfOOStack subclass: #FlagObject
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!FlagObject methodsFor: 'value' stamp: 'fhs 4/22/2023 22:48:57'!
value
	^value.! !


!FlagObject methodsFor: 'double dispatch auxiliar' stamp: 'JBT 4/24/2023 16:51:59'!
objectToBePoppedOnStack: StackGoingToBePopped
	"Si busco hacer pop y lo agarra el FlagObject significa que no hay nada en la pila"
	"Le pide a stackGoingToBePopped que mande el error"
	StackGoingToBePopped error: OOStack stackEmptyErrorDescription! !

!FlagObject methodsFor: 'double dispatch auxiliar' stamp: 'JBT 4/24/2023 16:52:11'!
objectToBeToppedOnStack: StackGoingToBeTopped
	"Si busco hacer top y lo agarra el FlagObject significa que no hay nada en la pila"
	"Le pide a stackGoingToBeTopped que mande el error"

	StackGoingToBeTopped error: OOStack stackEmptyErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FlagObject class' category: 'Stack-Exercise'!
FlagObject class
	instanceVariableNames: ''!

!FlagObject class methodsFor: 'as yet unclassified' stamp: 'fhs 4/23/2023 09:53:59'!
with: aValue 
	^FlagObject new initializeWith: aValue.! !


!classDefinition: #RegularObject category: 'Stack-Exercise'!
ObjectsOfOOStack subclass: #RegularObject
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!RegularObject methodsFor: 'value' stamp: 'fhs 4/22/2023 22:49:02'!
value
	^value.! !


!RegularObject methodsFor: 'double dispatch auxiliar' stamp: 'fhs 4/22/2023 22:57:27'!
objectToBePoppedOnStack: StackGoingToBePopped
	^StackGoingToBePopped removalOfObjectConfirmation
! !

!RegularObject methodsFor: 'double dispatch auxiliar' stamp: 'fhs 4/22/2023 23:14:16'!
objectToBeToppedOnStack: StackGoingToBeTopped
	^StackGoingToBeTopped procurementOfTopObjectConfirmation.
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RegularObject class' category: 'Stack-Exercise'!
RegularObject class
	instanceVariableNames: ''!

!RegularObject class methodsFor: 'as yet unclassified' stamp: 'fhs 4/22/2023 21:06:28'!
with: aValue 
	^RegularObject new initializeWith: aValue.! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'prefix checks' stamp: 'JBT 4/24/2023 16:48:07'!
checkPrefix: prefijoASerBuscado

	self prefixIsACharacterSequence: prefijoASerBuscado.
	self prefixIsNotEmpty: prefijoASerBuscado.
	self prefixDoesNotHaveSeparators: prefijoASerBuscado! !

!SentenceFinderByPrefix methodsFor: 'prefix checks' stamp: 'FHS 4/24/2023 15:25:51'!
prefixDoesNotHaveSeparators: prefijoASerEvaluado
	"Utilizamos separator para contar cualquier tipo de separacion (espacio, tab, carriage return, ...)"
	|separatorCheck|
	separatorCheck := ((prefijoASerEvaluado select: [:char | char isSeparator]) size) = 0.
	separatorCheck ifFalse: [self error: SentenceFinderByPrefix prefixSepatorErrorDescription].
	! !

!SentenceFinderByPrefix methodsFor: 'prefix checks' stamp: 'FHS 4/24/2023 15:36:01'!
prefixIsACharacterSequence: prefijoASerEvaluado
	(prefijoASerEvaluado isKindOf: CharacterSequence) ifFalse: [self error: SentenceFinderByPrefix prefixIsNotACharacterSequenceErrorDescription].! !

!SentenceFinderByPrefix methodsFor: 'prefix checks' stamp: 'FHS 4/24/2023 15:30:30'!
prefixIsNotEmpty: prefijoASerEvaluado
	(prefijoASerEvaluado isEmpty) ifTrue: [self error: SentenceFinderByPrefix prefixEmptyErrorDescription].
	! !


!SentenceFinderByPrefix methodsFor: 'finder' stamp: 'JBT 4/24/2023 16:49:31'!
findOnStack: unStackASerBuscado thePrefix: prefijoASerBuscado 
	
	self checkPrefix: prefijoASerBuscado.
	
	^self from: unStackASerBuscado select: [:elemento | elemento is: prefijoASerBuscado substringAt: 1]

! !

!SentenceFinderByPrefix methodsFor: 'finder' stamp: 'JBT 4/24/2023 16:49:17'!
from: stackABuscar  select: ifClosure
	
	
	"Nos quedamos sin tiempo para sacar el codigo repetido aca"
	|originalSize elementoPopeado stackTemp selection|
	originalSize := stackABuscar size.
	selection := OrderedCollection new.
	stackTemp := OOStack new.
	
	1 to: originalSize do: [:iter |
		elementoPopeado := stackABuscar pop.
		(ifClosure value: elementoPopeado ) ifTrue: [selection add: elementoPopeado].
		stackTemp push: elementoPopeado.
	].
	
	1 to: originalSize do: [:iter |
		elementoPopeado := stackTemp pop.
		stackABuscar push: elementoPopeado .
	].
	^selection
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'fhs 4/24/2023 10:16:47'!
prefixEmptyErrorDescription
	
	^ 'The prefix cannot be empty'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'fhs 4/24/2023 11:41:34'!
prefixIsNotACharacterSequenceErrorDescription
	
	^ 'The prefix needs to be a Character Sequence '! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'fhs 4/24/2023 10:43:05'!
prefixSepatorErrorDescription
	
	^ 'The prefix cannot have separators'! !
