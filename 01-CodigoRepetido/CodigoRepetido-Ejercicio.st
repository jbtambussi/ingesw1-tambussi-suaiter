!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'assertions' stamp: 'fd 4/12/2023 16:04:12'!
assertCanNotSuspendCustomer: aName inCustomerBook: customerBook andCheck: aClosureToCheck
	"Utilizamos andCheck para parametrizar el estado y poder utilizar el mensaje ya creado para los tests 03 y 04"
	self assertClosureFailure: [customerBook suspendCustomerNamed: aName. ] withExpectedError: CantSuspend toCheck: aClosureToCheck .! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'JBT 4/12/2023 15:40:17'!
assertClosure: aClosure RunsInLessThan: time
    | millisecondsBeforeRunning millisecondsAfterRunning |

    millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
    aClosure value.
    millisecondsAfterRunning := Time millisecondClockValue * millisecond.

    self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < time

    "Una opcion que vimos investigando operaciones sobre closures fue la de usar:
        self assert: (aClosure millisecondsToRun) < time
    Al usar esto nos     ahorraríamos todo lo anterior, y el código resulta + declarativo"! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'JBT 4/11/2023 16:13:15'!
assertClosureFailure: aClosure withExpectedError: exception toCheck: thingToDo
	[aClosure value. self fail]
		on: exception
		do: thingToDo
	! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'JBT 4/11/2023 16:54:13'!
assertCustomerBook: customerBook hasExpectedNumberOfActiveCustomers: expectedNumberOfActiveCustomers hasExpectedNumberOfSuspendedCustomers: expectedNumberOfSuspendedCustomers hasExpectedNumberOfTotalCustomers:expectedNumberOfTotalCustomers

	self assert: expectedNumberOfActiveCustomers equals: customerBook numberOfActiveCustomers.
	self assert: expectedNumberOfSuspendedCustomers equals: customerBook numberOfSuspendedCustomers.
	self assert: expectedNumberOfTotalCustomers equals: customerBook numberOfTotalCustomers.! !


!CustomerBookTest methodsFor: 'testing' stamp: 'JBT 4/12/2023 15:43:30'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds
    | customerBook  |
    customerBook := CustomerBook new.

    self assertClosure: [customerBook addCustomerNamed: 'John Lennon'.] RunsInLessThan: (50 * millisecond)
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'fd 4/12/2023 16:01:05'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

    | customerBook paulMcCartney |

    customerBook := CustomerBook new.
    paulMcCartney := 'Paul McCartney'.

    customerBook addCustomerNamed: paulMcCartney.
    self assertClosure: [customerBook removeCustomerNamed: paulMcCartney.] RunsInLessThan: (100 * millisecond)
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JBT 4/12/2023 15:44:16'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.
		
	self assertClosureFailure: [ customerBook addCustomerNamed: ''] 
		withExpectedError: Error
		toCheck:[:anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JBT 4/12/2023 15:44:25'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.

	self assertClosureFailure: [ customerBook removeCustomerNamed: 'Paul McCartney']
		withExpectedError: NotFound
		toCheck: [ :anError | 
			self assert: customerBook numberOfTotalCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon) ]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'fd 4/12/2023 16:06:19'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self assertCustomerBook: customerBook hasExpectedNumberOfActiveCustomers: 0 hasExpectedNumberOfSuspendedCustomers: 1 hasExpectedNumberOfTotalCustomers: 1.
	
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	"No incluimos la linea de arriba en el assertCustomerBook puesto que pensamos que son muy especificas al test en particular"
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'fd 4/12/2023 16:06:23'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assertCustomerBook: customerBook hasExpectedNumberOfActiveCustomers: 0 hasExpectedNumberOfSuspendedCustomers: 0 hasExpectedNumberOfTotalCustomers: 0.
	
	self deny: (customerBook includesCustomerNamed: paulMcCartney).
	"No incluimos la linea de arriba en el assertCustomerBook puesto que pensamos que son muy especificas al test en particular"


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JBT 4/12/2023 15:44:55'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.

	
	self assertCanNotSuspendCustomer: 'George Harrison' inCustomerBook: customerBook andCheck: [ :anError | 
			self assert: customerBook numberOfTotalCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon) ].
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JBT 4/12/2023 15:45:05'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self assertCanNotSuspendCustomer: johnLennon inCustomerBook: customerBook andCheck: [ :anError | 
			self assert: customerBook numberOfTotalCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon) ].
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspendedCustomers activeCustomers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'JBT 4/11/2023 17:08:22'!
includesCustomerNamed: aName

	^(activeCustomers includes: aName) or: [ suspendedCustomers includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'JBT 4/12/2023 15:38:23'!
isEmpty
	
	^self numberOfTotalCustomers = 0! !


!CustomerBook methodsFor: 'initialization' stamp: 'JBT 4/11/2023 17:08:22'!
initialize

	activeCustomers := OrderedCollection new.
	suspendedCustomers:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'fd 4/12/2023 15:57:13'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	
	(self includesCustomerNamed: aName) ifTrue: [self signalCustomerAlreadyExists].
	
	activeCustomers add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'JBT 4/11/2023 17:08:22'!
numberOfActiveCustomers
	
	^activeCustomers size! !

!CustomerBook methodsFor: 'customer management' stamp: 'JBT 4/11/2023 17:08:11'!
numberOfSuspendedCustomers
	
	^suspendedCustomers size! !

!CustomerBook methodsFor: 'customer management' stamp: 'JBT 4/12/2023 15:37:22'!
numberOfTotalCustomers

	^self numberOfActiveCustomers + self numberOfSuspendedCustomers! !

!CustomerBook methodsFor: 'customer management' stamp: 'fd 4/12/2023 15:58:09'!
removeCustomerNamed: aName 
"Viendo los mensajes de OrderedCollection vimos que hay un mensaje que hace lo que queremos.
 Remueve el elemento con un codigo muy similar. Todo este metodo puede ser escrito de la siguiente forma:"

activeCustomers remove: aName ifAbsent: [
	suspendedCustomers remove: aName ifAbsent:[
		^NotFound signal
		]
	]


! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'JBT 4/11/2023 17:08:22'!
suspendCustomerNamed: aName 
	
	(activeCustomers includes: aName) ifFalse: [^CantSuspend signal].
	
	activeCustomers remove: aName.
	
	suspendedCustomers add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/9/2023 22:25:52'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/9/2023 22:25:56'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
