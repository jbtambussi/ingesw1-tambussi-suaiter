!classDefinition: #ShoppingCartTest category: 'TusLibros-Iteracion1'!
TestCase subclass: #ShoppingCartTest
	instanceVariableNames: 'shoppingCart'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Iteracion1'!

!ShoppingCartTest methodsFor: 'tests' stamp: 'FHS 6/8/2023 11:43:30'!
test01newShoppingCartIsEmpty
	|cart|
	cart := ShoppingCart withAvailableItems: OrderedCollection new.
	self assert: cart isEmpty! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FHS 6/8/2023 11:33:11'!
test02addingSomethingToTheCartMakesItNoLongerEmpty
	|cart|
	cart := ShoppingCart withAvailableItems: (OrderedCollection with: '9783161484100').
	
	cart addItem: '9783161484100' withQuantity: 1. 
	self deny: cart isEmpty.! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FHS 6/8/2023 11:33:37'!
test03canAddMultipleThingsToTheCart
	|cart|
	cart := ShoppingCart withAvailableItems: (OrderedCollection with: '9783161484100' with: '9783161484101').

	cart addItem: '9783161484100' withQuantity: 1. 
	cart addItem: '9783161484101' withQuantity: 1. 
	self assert: 2 equals: cart size.! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FHS 6/8/2023 11:34:13'!
test04cartCanListItsItemsWithRespectiveAmmounts
	|cart|
	cart := ShoppingCart withAvailableItems: (OrderedCollection with: '9783161484100' with: '9783161484101').

	cart addItem: '9783161484100' withQuantity: 1. 
	cart addItem: '9783161484101' withQuantity: 2. 
	self 
		assert: (OrderedCollection with: '9783161484100|1' with: '9783161484101|2') 
		equals: cart listItems.! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FHS 6/8/2023 11:48:53'!
test05cannotAddANonSellableItemToTheCart
	|cart|
	cart := ShoppingCart withAvailableItems: (OrderedCollection with: '9783161484100').

	self should: [cart addItem: '9783161484101' withQuantity: 2.]
		raise: Error
		withExceptionDo: [:exception |
			self assert: ShoppingCart cannotAddAnItemNotAvailableErrorMessage equals: exception messageText.
			self assert: cart isEmpty.
		].
! !

!ShoppingCartTest methodsFor: 'tests' stamp: 'FHS 6/8/2023 11:50:07'!
test06cannotAddAnItemToTheCartWithQuantityLessThanOne
	|cart|
	cart := ShoppingCart withAvailableItems: (OrderedCollection with: '9783161484100').

	self should: [cart addItem: '9783161484100' withQuantity: -1.]
		raise: Error
		withExceptionDo: [:exception |
			self assert: ShoppingCart cannotAddAnItemWithQuantityLessThanOneErrorMessage equals: exception messageText.
			self assert: cart isEmpty.
		].
! !


!classDefinition: #ShoppingCart category: 'TusLibros-Iteracion1'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'itemsInside itemsAvailable'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Iteracion1'!

!ShoppingCart methodsFor: 'initialization' stamp: 'FHS 6/8/2023 11:39:12'!
initializeWithAvailableItems: aListOfItems
	itemsInside := Bag new.
	itemsAvailable := aListOfItems.! !


!ShoppingCart methodsFor: 'testing' stamp: 'FHS 6/8/2023 10:56:21'!
isEmpty
	^itemsInside isEmpty.! !

!ShoppingCart methodsFor: 'testing' stamp: 'FHS 6/8/2023 10:55:58'!
size
	^itemsInside size.! !


!ShoppingCart methodsFor: 'add' stamp: 'FHS 6/8/2023 11:56:21'!
addItem: anItem withQuantity: aQuantity
	self assertValidItem: anItem andValidQuantity: aQuantity.
	
	itemsInside add: anItem withOccurrences: aQuantity.

"Fue confuso la explicacion de si estamos modelando un carro de libros o
algo mas generico como un carro de supermercado. Por eso terminamos poniendolo como items"
! !


!ShoppingCart methodsFor: 'list' stamp: 'FHS 6/8/2023 11:18:23'!
listItems
	|list x|
	list := OrderedCollection new.
	x := itemsInside sortedElements.
	x do: [:pairKeyValue |
		list addLast: pairKeyValue key , '|' , pairKeyValue value printString.
		].
	^list.
	
	"No utilizamos printString en la key puesto que asumimos que la misma ya es uno"! !


!ShoppingCart methodsFor: 'assertions' stamp: 'FHS 6/8/2023 11:55:18'!
assertItemsIsAvailable: anItem

	^ (itemsAvailable includes: anItem) ifFalse: [self error: self class cannotAddAnItemNotAvailableErrorMessage]! !

!ShoppingCart methodsFor: 'assertions' stamp: 'FHS 6/8/2023 11:54:46'!
assertQuantityIsNotLessThanOne: aQuantity

	^ (aQuantity < 1) ifTrue: [self error: self class cannotAddAnItemWithQuantityLessThanOneErrorMessage ]! !

!ShoppingCart methodsFor: 'assertions' stamp: 'FHS 6/8/2023 11:55:53'!
assertValidItem: anItem andValidQuantity: aQuantity

	self assertItemsIsAvailable: anItem.
	self assertQuantityIsNotLessThanOne: aQuantity! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: 'TusLibros-Iteracion1'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'instance creation' stamp: 'FHS 6/8/2023 11:38:27'!
withAvailableItems: aListOfItems
	^self new initializeWithAvailableItems: aListOfItems.
! !


!ShoppingCart class methodsFor: 'error message' stamp: 'FHS 6/8/2023 11:41:30'!
cannotAddAnItemNotAvailableErrorMessage
	^'item not available'! !

!ShoppingCart class methodsFor: 'error message' stamp: 'FHS 6/8/2023 11:51:05'!
cannotAddAnItemWithQuantityLessThanOneErrorMessage
	^'cannot add item with quantity less than one'! !
