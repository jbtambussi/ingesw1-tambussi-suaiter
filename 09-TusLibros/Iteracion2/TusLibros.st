!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'support' stamp: 'FHS 6/12/2023 11:21:04'!
createCart
	
	^Cart withAPriceList: self defaultPriceList.! !

!TusLibrosTest methodsFor: 'support' stamp: 'FHS 6/12/2023 11:13:02'!
defaultPriceList
	|priceList|
	priceList := Dictionary new.
	priceList at: 'validBook' put: 1.
	^priceList ! !

!TusLibrosTest methodsFor: 'support' stamp: 'FHS 6/12/2023 11:13:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TusLibrosTest methodsFor: 'support' stamp: 'FHS 6/12/2023 11:13:13'!
itemSellByTheStore
	
	^ 'validBook'! !


!classDefinition: #CartTest category: 'TusLibros'!
TusLibrosTest subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: self itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: self itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self assert: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: self itemSellByTheStore.
	self assert: (cart occurrencesOf: self itemSellByTheStore) = 2! !


!classDefinition: #CashierTest category: 'TusLibros'!
TusLibrosTest subclass: #CashierTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:26:44'!
test01cannotDoACheckOutOfAnEmptyCart
	| cart cashier aCreditCard |
	
	cart := self createCart.
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createValidCreditCard.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: Cashier cannotCheckoutAnEmptyCartErrorMessage equals: error messageText.
			self assert: cart isEmpty.
		].
! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:26:44'!
test02checkOutOfAnItemReturnsTotal
	| cart cashier aCreditCard |
	
	cart := self createCart.
	cart add: 5 of: self itemSellByTheStore.
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createValidCreditCard.
	
	self assert: 5 equals: (cashier checkOutCart: cart withACreditCard: aCreditCard). 
! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:26:44'!
test03checkOutOfMultipeItemsReturnsTotal
	| cart cashier priceList aCreditCard |	
	
	priceList := Dictionary new.
	priceList at: 'Potato' put: 10.
	priceList at: 'Tomato' put: 100.
	
	cart := Cart withAPriceList: priceList.
	cart add: 5 of: 'Potato'.
	cart add: 1 of: 'Tomato'.
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createValidCreditCard.
	
	self assert: 150 equals: (cashier checkOutCart: cart withACreditCard: aCreditCard).
! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:59'!
test04cannotCheckOutWithAnExpiredCard
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createExpiredCreditCard.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: Cashier expiredCardErrorMessage equals: error messageText.
		].

! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:52'!
test05cannotCheckOutWithACardWithout16digits
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createNotEnoughDigitsCreditCard.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: Cashier incorrectNumberOfDigitsOfCardErrorMessage equals: error messageText.
		].

! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:45'!
test06cannotCheckOutWithACardWithoutNumber
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createCreditCardWithNotANumber.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: Cashier cardNumberMustBeANumberErrorMessage equals: error messageText.
		].

! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:38'!
test07cannotCheckOutWithACardWithoutOwner
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createCreditCardWithNoOwnerName.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: Cashier cardOwnerMustNotBeEmptyErrorMessage equals: error messageText.
		].

! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:29'!
test08afterCheckOutCartIsEmpty
	"Este test se le comento a un docente y nos dijo que es algo que deberia de pasar."
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createValidCreditCard.
	
	self deny: cart isEmpty.
	cashier checkOutCart: cart withACreditCard: aCreditCard.
	self assert: cart isEmpty.
! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:23'!
test09cannotCheckOutWithACardWithoutCredit
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createCreditCardWithoutCredit.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: 'Card without credit' equals: error messageText.
			]
		
	"No estamos seguros si el cashier tiene que levantar el error o si simplemente debe pasar el error del merchant processor"! !

!CashierTest methodsFor: 'tests' stamp: 'FHS 6/12/2023 11:30:14'!
test10cannotCheckOutWithAStolenCard
	| cart cashier aCreditCard |	
	
	cart := self createCartWith5PotatoesWithPrice10.
	
	cashier := Cashier with: MerchantProcessorSimulator new.
	
	aCreditCard := self createStolenCreditCard.
	
	self should: [cashier checkOutCart: cart withACreditCard: aCreditCard]
		raise: Error
		withExceptionDo: [:error |
			self assert: 'Card stolen' equals: error messageText.
			]
		
	"No estamos seguros si el cashier tiene que levantar el error o si simplemente debe pasar el error del merchant processor"! !


!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 11:29:55'!
createCartWith5PotatoesWithPrice10
	
	| cart priceList |
	priceList := Dictionary new.
	priceList at: 'Potato' put: 10.
	
	cart := Cart withAPriceList: priceList.
	cart add: 5 of: 'Potato'.
	
	^cart! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 10:12:44'!
createCreditCardWithNoOwnerName
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	aCreditCard at: 'number' put: 4078931347814879.
	aCreditCard at: 'owner' put: ''.
	^aCreditCard ! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 10:12:08'!
createCreditCardWithNotANumber
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	aCreditCard at: 'number' put: 'xxxxxxxyyyyyyy'.
	aCreditCard at: 'owner' put: 'Roberto White'.
	^aCreditCard ! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 10:33:12'!
createCreditCardWithoutCredit
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	aCreditCard at: 'number' put: 4078000047810000.
	aCreditCard at: 'owner' put: 'Roberto Black'.
	^aCreditCard ! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 10:08:01'!
createExpiredCreditCard
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2020)).
	aCreditCard at: 'number' put: 4078931347814879.
	aCreditCard at: 'owner' put: 'Roberto White'.
	^aCreditCard ! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 10:09:51'!
createNotEnoughDigitsCreditCard
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	aCreditCard at: 'number' put: 40789.
	aCreditCard at: 'owner' put: 'Roberto White'.
	^aCreditCard ! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 11:02:11'!
createStolenCreditCard
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	aCreditCard at: 'number' put: 4078000099991111.
	aCreditCard at: 'owner' put: 'Roberto Black'.
	^aCreditCard ! !

!CashierTest methodsFor: 'support - create' stamp: 'FHS 6/12/2023 10:06:33'!
createValidCreditCard
	|aCreditCard|
	aCreditCard := Dictionary new.
	aCreditCard at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	aCreditCard at: 'number' put: 4078931347814879.
	aCreditCard at: 'owner' put: 'Roberto White'.
	^aCreditCard ! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items priceList'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'FHS 6/12/2023 11:19:23'!
assertIsValidItem: anItem

	(priceList keys includes: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'FHS 6/12/2023 11:20:36'!
withAPriceList: aPriceList 

	items := OrderedCollection new.
	priceList := aPriceList.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'FHS 6/11/2023 17:07:05'!
priceOf: anItem
	^priceList at: anItem "Agregar ifAbsent para controlar error"! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'check out' stamp: 'FHS 6/12/2023 10:18:20'!
clearCart
	items:= OrderedCollection new.! !

!Cart methodsFor: 'check out' stamp: 'FHS 6/12/2023 11:26:32'!
totalCheckOut
	^items sum: [:anItem| self priceOf: anItem 		] ifEmpty: [self shouldNotHappen ].
	
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'FHS 6/12/2023 11:21:04'!
withAPriceList: aPriceList 

	^self new withAPriceList: aPriceList ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'assertions' stamp: 'FHS 6/11/2023 18:02:11'!
assertExpirationDate: aCardExpirationDate

	^ (aCardExpirationDate < self today) ifTrue: [self error: self class expiredCardErrorMessage]! !

!Cashier methodsFor: 'assertions' stamp: 'FHS 6/11/2023 18:02:03'!
assertOwner: aCardOwner
	aCardOwner isEmpty ifTrue: [self error: self class cardOwnerMustNotBeEmptyErrorMessage].! !

!Cashier methodsFor: 'assertions' stamp: 'FHS 6/11/2023 18:01:30'!
assertValidCard: aCreditCard

	self assertExpirationDate: (aCreditCard at: 'expirationDate').
	self assertValidCreditCardNumber: (aCreditCard at: 'number').
	self assertOwner: (aCreditCard at: 'owner').! !

!Cashier methodsFor: 'assertions' stamp: 'FHS 6/11/2023 18:01:51'!
assertValidCreditCardNumber: aCardNumber
	(aCardNumber isKindOf: Number) ifFalse:  [self error: self class cardNumberMustBeANumberErrorMessage].
	(((aCardNumber printString size) = 16)) ifFalse: [self error: self class incorrectNumberOfDigitsOfCardErrorMessage].
	! !


!Cashier methodsFor: 'initialization' stamp: 'FHS 6/12/2023 10:42:23'!
initializeWith: aMerchantProcessor
	merchantProcessor := aMerchantProcessor .! !


!Cashier methodsFor: 'check out' stamp: 'FHS 6/12/2023 11:27:36'!
checkOutCart: aCart withACreditCard: aCreditCard 
	|totalRecibo|
	self assertValidCard: aCreditCard.
	
	aCart isEmpty ifTrue: [self error: self class cannotCheckoutAnEmptyCartErrorMessage.].
	
	totalRecibo := aCart totalCheckOut.
	
	[merchantProcessor debit: totalRecibo from: aCreditCard] 
		on: Error
		do: [:anError |
			anError pass
			]. "Leer comentario de los tests"
	
	aCart clearCart.
	
	^totalRecibo
	
	! !


!Cashier methodsFor: 'date' stamp: 'FHS 6/11/2023 17:29:37'!
today
	^GregorianMonthOfYear julyOf: (GregorianYear createYear: 2023).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'error messages' stamp: 'FHS 6/11/2023 16:44:48'!
cannotCheckoutAnEmptyCartErrorMessage
	^'The cart must have at least one item'! !

!Cashier class methodsFor: 'error messages' stamp: 'FHS 6/11/2023 17:55:36'!
cardNumberMustBeANumberErrorMessage
	^'The card number must be a number'! !

!Cashier class methodsFor: 'error messages' stamp: 'FHS 6/11/2023 18:00:27'!
cardOwnerMustNotBeEmptyErrorMessage
	^'Owner name must not be empty'! !

!Cashier class methodsFor: 'error messages' stamp: 'FHS 6/11/2023 17:27:10'!
expiredCardErrorMessage
	^'The card has expired'! !

!Cashier class methodsFor: 'error messages' stamp: 'FHS 6/11/2023 17:39:40'!
incorrectNumberOfDigitsOfCardErrorMessage
	^'The card number must have 16 digits'! !


!Cashier class methodsFor: 'instance creation' stamp: 'FHS 6/12/2023 10:41:56'!
with: aMerchantProcessor
	^self new initializeWith: aMerchantProcessor .! !


!classDefinition: #MerchantProcessorSimulator category: 'TusLibros'!
Object subclass: #MerchantProcessorSimulator
	instanceVariableNames: 'cardWithoutFunds cardStolen'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorSimulator methodsFor: 'initialization' stamp: 'FHS 6/12/2023 11:10:26'!
createCardThatHasBeenStolen
	cardStolen := Dictionary new.
	cardStolen at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	cardStolen at: 'number' put: 4078000099991111.
	cardStolen at: 'owner' put: 'Roberto Black'! !

!MerchantProcessorSimulator methodsFor: 'initialization' stamp: 'FHS 6/12/2023 11:09:22'!
createCardWithoutFunds
	cardWithoutFunds := Dictionary new.
	cardWithoutFunds at: 'expirationDate' put: (GregorianMonthOfYear julyOf: (GregorianYear createYear: 2025)).
	cardWithoutFunds at: 'number' put: 4078000047810000.
	cardWithoutFunds at: 'owner' put: 'Roberto Black'.! !

!MerchantProcessorSimulator methodsFor: 'initialization' stamp: 'FHS 6/12/2023 11:10:30'!
initialize
	"Desconocemos como se comparan las tarjetas en el simulador, asi que decidimos comparar por igualdad de la estructura 
	(siguiendo la idea de hacer un simulador sencillo pedido en la consigna de esta 2da iteracion)"

	self createCardWithoutFunds.
	self createCardThatHasBeenStolen.

	
	! !


!MerchantProcessorSimulator methodsFor: 'debit' stamp: 'FHS 6/12/2023 11:03:44'!
debit: anAmount from: aCreditCard
	(aCreditCard = cardWithoutFunds) ifTrue: [self error: 'Card without credit'].
	(aCreditCard = cardStolen) ifTrue: [self error: 'Card stolen'].! !
