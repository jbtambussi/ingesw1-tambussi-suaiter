!classDefinition: #PortfolioTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'operations' stamp: 'FHS 5/21/2023 19:21:41'!
depositOf10

	^ Deposit for: 10! !

!PortfolioTest methodsFor: 'operations' stamp: 'FHS 5/21/2023 19:21:30'!
withdrawOf10

	^ Withdraw for: 10! !


!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/20/2023 15:48:23'!
test01EmptyPortfolioHasBalanceZero
	|emptyPortfolio|
	emptyPortfolio := Portfolio new.
	self assert: 0 equals: emptyPortfolio balance.! !

!PortfolioTest methodsFor: 'tests' stamp: 'FHS 5/21/2023 19:21:52'!
test02BalanceOfPortfolioWithOnlyAccounts
	|portfolio accountAGimena accountBPatricio accountCMarcelo |
	
	portfolio := Portfolio new.
	accountAGimena := ReceptiveAccount new.
	accountBPatricio := ReceptiveAccount new.
	accountCMarcelo := ReceptiveAccount new.
	
	accountAGimena register: self depositOf10.
	accountBPatricio register: self depositOf10 .
	accountCMarcelo register: self withdrawOf10 .
	
	portfolio addMember: accountAGimena ; addMember: accountBPatricio ; addMember: accountCMarcelo.
	self assert: 10 equals: portfolio balance.! !

!PortfolioTest methodsFor: 'tests' stamp: 'FHS 5/21/2023 19:22:00'!
test03BalanceOfPortfolioWithAccountsAndPortfolios
	|portfolio portfolioIntern accountAGimena accountBPatricio accountCMarcelo |
	
	portfolio := Portfolio new.
	portfolioIntern := Portfolio new.
 	accountAGimena := ReceptiveAccount new.
	accountBPatricio := ReceptiveAccount new.
	accountCMarcelo := ReceptiveAccount new.
	
	accountAGimena register: self depositOf10.
	accountBPatricio register: self depositOf10 .
	accountCMarcelo register: self withdrawOf10 .
	
	portfolioIntern addMember: accountCMarcelo.
	
	portfolio addMember: accountAGimena ; addMember: accountBPatricio ; addMember: portfolioIntern.
	self assert: 10 equals: portfolio balance.! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/20/2023 16:09:41'!
test04EmptyPortfolioHasNoTransactions
	|emptyPortfolio|
	emptyPortfolio := Portfolio new.
	self assert: Bag new equals: emptyPortfolio transactions.! !

!PortfolioTest methodsFor: 'tests' stamp: 'FHS 5/21/2023 19:27:12'!
test05TransactionsOfPortfolioWithOnlyAccounts
	|portfolio accountAGimena accountBPatricio accountCMarcelo depositFor10 withdrawFor10|
	
	portfolio := Portfolio new.
	accountAGimena := ReceptiveAccount new.
	accountBPatricio := ReceptiveAccount new.
	accountCMarcelo := ReceptiveAccount new.
	
	depositFor10 := self depositOf10.
	withdrawFor10 := self withdrawOf10.
	
	accountAGimena register: depositFor10.
	accountBPatricio register: depositFor10 .
	accountCMarcelo register: withdrawFor10 .
	
	portfolio addMember: accountAGimena ; addMember: accountBPatricio ; addMember: accountCMarcelo.
	self assert: (Bag with: depositFor10 with: depositFor10 with: withdrawFor10) equals: portfolio transactions.
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'FHS 5/21/2023 19:27:02'!
test06TransactionsOfPortfolioWithAccountsAndPortfolios
	|portfolio portfolioIntern accountAGimena accountBPatricio accountCMarcelo depositFor10 withdrawFor10|
	
	portfolio := Portfolio new.
	portfolioIntern := Portfolio new.
 	accountAGimena := ReceptiveAccount new.
	accountBPatricio := ReceptiveAccount new.
	accountCMarcelo := ReceptiveAccount new.
	
	depositFor10 := self depositOf10.
	withdrawFor10 := self withdrawOf10.
	
	accountAGimena register: depositFor10.
	accountBPatricio register: depositFor10 .
	accountCMarcelo register: withdrawFor10 .
	
	portfolioIntern addMember: accountCMarcelo.
	
	portfolio addMember: accountAGimena ; addMember: accountBPatricio ; addMember: portfolioIntern.
	self assert: (Bag with: depositFor10 with: depositFor10 with: withdrawFor10) equals: portfolio transactions .! !

!PortfolioTest methodsFor: 'tests' stamp: 'FHS 5/21/2023 19:28:20'!
test07EmptyPortfolioHasRegisteredIsFalse
	|emptyPortfolio |
	emptyPortfolio := Portfolio new.
	self deny: (emptyPortfolio hasRegistered: self withdrawOf10) .! !

!PortfolioTest methodsFor: 'tests' stamp: 'FHS 5/21/2023 19:29:30'!
test08PortfolioWithAccountsAndPortfoliosHasRegisteredATransaction
	|portfolio portfolioIntern accountAGimena accountBPatricio accountCMarcelo depositFor10 withdrawFor10|
	
	portfolio := Portfolio new.
	portfolioIntern := Portfolio new.
 	accountAGimena := ReceptiveAccount new.
	accountBPatricio := ReceptiveAccount new.
	accountCMarcelo := ReceptiveAccount new.
	
	depositFor10 := self depositOf10.
	withdrawFor10 := self withdrawOf10.
	
	accountAGimena register: depositFor10.
	accountBPatricio register: depositFor10 .
	accountCMarcelo register: withdrawFor10 .
	
	portfolioIntern addMember: accountCMarcelo.
	
	portfolio addMember: accountAGimena ; addMember: accountBPatricio ; addMember: portfolioIntern.
	self assert:( portfolio hasRegistered: withdrawFor10 )! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:49'!
test09CantAddTheSameAccountTwiceInAPortfolioWithoutNestedPortfolios
	|portfolio account|
"	     p
          /      \
account     [account]
	 trying to add account to p shouldnt work
"	
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	portfolio addMember: account .
	
	self should: [portfolio addMember: account]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:43'!
test10CantAddAnAccountThatIsInANestedPortfolio
	|parentPortfolio portfolio account|
"  pp
 /     \ 
p    [account] --> trying to add account to pp shouldnt work
|
account
"
	parentPortfolio := Portfolio new.
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	
	portfolio addMember: account.
	parentPortfolio addMember: portfolio.
	
	self should: [parentPortfolio addMember: account]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:34'!
test11CantAddAPortfolioThatIsInANestedPortfolio
	|parentPortfolio portfolio account|
"  pp
 /     \ 
p    [p] --> trying to add p to pp again shouldnt work
|     
account
"
	parentPortfolio := Portfolio new.
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	
	portfolio addMember: account.
	parentPortfolio addMember: portfolio.
	
	self should: [parentPortfolio addMember: account]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:25'!
test12CantAddAMemberThatIsInAPredecesorPortfolio
	| account parentPortfolio portfolio |
"	      pp
  	    /      \
  	   p        account
   	   |
	[account] ---> trying to add account to p shouldnt work
"
	parentPortfolio := Portfolio new.
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	
	parentPortfolio addMember: account; addMember: portfolio .
	
	self should: [portfolio addMember: account]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:18'!
test13CantAddAMemberThatIsInAnyPredecesorPortfolio
	| account parentPortfolio1 parentPortfolio2 portfolio |
"pp2   pp1
    \    /      \
     p        account
      |
[account]  --> trying to add account to p shouldnt work
"
	parentPortfolio1 := Portfolio new.
	parentPortfolio2 := Portfolio new.
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	
	parentPortfolio1 addMember: account; addMember: portfolio.
	parentPortfolio2 addMember: portfolio.
	
	self should: [portfolio addMember: account]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]
					! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:10'!
test14CantAddAPortfolioThatHasAnAccountInAPredecesorPortfolio
	| account parentPortfolio  portfolio |
"          pp
         /      \
account   [p]
               /
        account
"
	parentPortfolio := Portfolio new.
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	
	parentPortfolio addMember: account.
	portfolio addMember: account.
	
	self should: [parentPortfolio addMember: portfolio.]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]
					! !

!PortfolioTest methodsFor: 'tests' stamp: 'JBT 5/22/2023 14:41:02'!
test15CantAddAnAccountDeepOnTheTreeThatIsInAPredecesorPortfolio
	| c1 p1 p2 p3 p4  |
"    p1    p2 
        \    /    \
 	p3      c1
           \
          p4
	   \
	  [c1]
"
	p1 := Portfolio new.
	p2 := Portfolio new.
	p3 := Portfolio new.
	p4 := Portfolio new.
	c1 := ReceptiveAccount new.
	
	p2 addMember: c1.
	p2 addMember: p3.
	
	p1 addMember: p3.
	
	p3 addMember: p4.
	
	self should: [p4 addMember: c1.]
		raise: Error
		withExceptionDo: [:anError| self assert: anError messageText equals: Portfolio memberAlreadyAddedErrorDescription.]
					! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/17/2021 17:29:53'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'JBT 5/20/2023 15:44:25'!
affectBalance: previousBalance
	^previousBalance + value! !

!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'JBT 5/20/2023 15:44:36'!
affectBalance: previousBalance
	^previousBalance - value! !

!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
Object subclass: #Portfolio
	instanceVariableNames: 'members parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'add' stamp: 'FHS 5/21/2023 22:48:23'!
addMember: aMemberToBeAdded
	
	self assertMemberIsNotAlredyOnTheTree: aMemberToBeAdded.
	
	members add: aMemberToBeAdded .
	aMemberToBeAdded addParent: self.! !

!Portfolio methodsFor: 'add' stamp: 'JBT 5/21/2023 16:33:47'!
addParent: portfolioToBeParent
	parents add: portfolioToBeParent ! !

!Portfolio methodsFor: 'add' stamp: 'FHS 5/21/2023 22:46:25'!
assertMemberIsNotAlredyOnTheTree: aMemberToBeChecked
	|rootsOfAllMembers membersOfaMemberToBeAdded |
	
	rootsOfAllMembers := self findAllMembersOfAllRoots.
	membersOfaMemberToBeAdded := aMemberToBeChecked findAllMembers.
	
	(rootsOfAllMembers intersection: membersOfaMemberToBeAdded) ifNotEmpty: [self error: self class memberAlreadyAddedErrorDescription].! !


!Portfolio methodsFor: 'balance' stamp: 'JBT 5/21/2023 16:42:01'!
balance
	^(members sum: [:member | member balance] ifEmpty: [0]) ! !


!Portfolio methodsFor: 'transactions' stamp: 'JBT 5/20/2023 16:52:53'!
hasRegistered: aTransactionToCheck
	^members anySatisfy: [:member | member hasRegistered: aTransactionToCheck ].! !

!Portfolio methodsFor: 'transactions' stamp: 'FHS 5/21/2023 20:48:03'!
transactions
	^ members inject: Bag new 
				into: [:bag :member | bag addAll: member transactions; yourself ].! !


!Portfolio methodsFor: 'initialization' stamp: 'JBT 5/21/2023 16:34:17'!
initialize
	members := Set new.
	parents := Set new.! !


!Portfolio methodsFor: 'testing' stamp: 'JBT 5/22/2023 14:39:02'!
checkIfCollectionOfPortfoliosAndClients: aCollection has: aMemberToCheck

	^ members anySatisfy: [:member| member hasOrIsMember: aMemberToCheck ]! !

!Portfolio methodsFor: 'testing' stamp: 'JBT 5/22/2023 14:39:40'!
checkIfMembersHas: aMemberToCheck

	^self checkIfCollectionOfPortfoliosAndClients: members has: aMemberToCheck ! !

!Portfolio methodsFor: 'testing' stamp: 'JBT 5/22/2023 14:39:57'!
hasOrIsMember: aMemberToBeChecked
	^self == aMemberToBeChecked  or: 
	[self checkIfMembersHas: aMemberToBeChecked].! !

!Portfolio methodsFor: 'testing' stamp: 'JBT 5/22/2023 14:39:02'!
rootsHaveThisMember: aMemberToCheck
	| roots |
	roots :=self findRootPortfolios.
	^(self checkIfCollectionOfPortfoliosAndClients: roots has: aMemberToCheck)! !


!Portfolio methodsFor: 'find' stamp: 'FHS 5/21/2023 22:36:22'!
findAllMembers
	^members inject: (Set with: self) 
			into: [:allMembers :member | allMembers union: member findAllMembers].
	
	! !

!Portfolio methodsFor: 'find' stamp: 'FHS 5/21/2023 22:38:14'!
findAllMembersOfAllRoots
	|roots|
	roots := self findRootPortfolios.
	^roots inject: (Set new) 
			into: [:allMembers :root | allMembers union: root findAllMembers].
	
	! !

!Portfolio methodsFor: 'find' stamp: 'FHS 5/21/2023 22:34:20'!
findRootPortfolios
	^parents inject: (Set with: self) 
			into: [:roots :parent | roots union: parent findRootPortfolios].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'error description' stamp: 'JBT 5/21/2023 15:51:42'!
memberAlreadyAddedErrorDescription
	^'The member is already in the portfolio'! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
Object subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'NR 10/17/2019 15:06:56'!
initialize

	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'JBT 5/20/2023 15:45:26'!
balance

	^transactions inject: 0 into: [:total :transaction | transaction affectBalance: total]! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'FHS 5/21/2023 19:36:14'!
findAllMembers
	^Set with: self! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'JBT 5/22/2023 14:23:38'!
hasOrIsMember: anAccountToBeChecked
	^self == anAccountToBeChecked ! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'add' stamp: 'FHS 5/21/2023 20:19:54'!
addParent: portfolioToBeParent
	"No hago nada puesto que el hijo no requiere conocer a su padre"! !
