!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head observers updater'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:20:18'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	
	updater := Updater new.
	observers := OrderedCollection new.! !


!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:17:02'!
headEast
	
	head := MarsRoverHeadingEast for: self! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:17:12'!
headNorth
	
	head := MarsRoverHeadingNorth for: self ! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:17:17'!
headSouth
	
	head := MarsRoverHeadingSouth for: self! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:17:24'!
headWest
	
	head := MarsRoverHeadingWest for: self ! !

!MarsRover methodsFor: 'heading' stamp: 'FHS 6/4/2023 17:20:59'!
rotateLeft
	
	head rotateLeft.
	updater updateDirection: head
		
! !

!MarsRover methodsFor: 'heading' stamp: 'FHS 6/4/2023 17:21:04'!
rotateRight
	
	head rotateRight.
	updater updateDirection: head! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'FHS 6/4/2023 17:20:31'!
moveBackward
	
	head moveBackward.
	updater updatePosition: position.

	! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 8/22/2019 12:15:01'!
moveEast
	
	position := position + (1@0)! !

!MarsRover methodsFor: 'moving' stamp: 'FHS 6/4/2023 17:20:39'!
moveForward
	
	head moveForward.
	updater updatePosition: position.

	! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 8/22/2019 12:13:12'!
moveNorth
	
	position := position + (0@1)! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 8/22/2019 12:13:34'!
moveSouth
	
	position := position + (0@-1)! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 8/22/2019 12:15:32'!
moveWest
	
	position := position + (-1@0)! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'observer' stamp: 'FHS 6/3/2023 12:05:41'!
addObserver: anObserver
	updater addObserver: anObserver.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!classDefinition: #Observers category: 'MarsRover-WithHeading'!
Object subclass: #Observers
	instanceVariableNames: 'rover record'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!
!Observers commentStamp: '<historical>' prior: 0!
Para evitar que haya codigo repetido, las subclases heredan el funcionamiento de los update que no hacen nada. En caso requerir el update para poder realizar su funcion, redefinen la funcion evitando asi utilizar el "update vacio". De esta manera al querer agregar un observer no necesita definir nuevamente todos los updates anteriores.!


!Observers methodsFor: 'update' stamp: 'FHS 6/4/2023 17:24:22'!
updateDirection: aCardinalPoint
	"Leer comentario de clase (class comment)"! !

!Observers methodsFor: 'update' stamp: 'FHS 6/4/2023 17:24:17'!
updatePosition: aPosition
	"Leer comentario de clase (class comment)"! !


!Observers methodsFor: 'show' stamp: 'FHS 6/4/2023 17:13:08'!
show
	^record! !


!Observers methodsFor: 'initialization' stamp: 'FHS 6/4/2023 15:53:26'!
initializeOf: aMarsRover
	self subclassResponsibility ! !


!classDefinition: #HeadingAndPositionLog category: 'MarsRover-WithHeading'!
Observers subclass: #HeadingAndPositionLog
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadingAndPositionLog methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:11:16'!
initializeOf: aMarsRover
	rover := aMarsRover.
	record := OrderedCollection new.
	
	aMarsRover addObserver: self.! !


!HeadingAndPositionLog methodsFor: 'update' stamp: 'FHS 6/4/2023 17:11:16'!
updateDirection: aCardinalPoint
	record addLast: aCardinalPoint.! !

!HeadingAndPositionLog methodsFor: 'update' stamp: 'FHS 6/4/2023 17:11:16'!
updatePosition: aPosition
	record addLast: aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeadingAndPositionLog class' category: 'MarsRover-WithHeading'!
HeadingAndPositionLog class
	instanceVariableNames: ''!

!HeadingAndPositionLog class methodsFor: 'instance creation' stamp: 'FHS 6/4/2023 15:58:44'!
of: aMarsRover
	^self new initializeOf: aMarsRover.! !


!classDefinition: #HeadingAndPositionWindow category: 'MarsRover-WithHeading'!
Observers subclass: #HeadingAndPositionWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadingAndPositionWindow methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:11:34'!
initializeOf: aMarsRover
	rover := aMarsRover.
	record := OrderedCollection with: #NotMovedYet with: #NotMovedYet.
	"La primera representa la position y la segunda el heading"
	
	aMarsRover addObserver: self.! !


!HeadingAndPositionWindow methodsFor: 'update' stamp: 'FHS 6/4/2023 17:11:34'!
updateDirection: aCardinalPoint
	record at: 2 put: ('Pointing ', aCardinalPoint).! !

!HeadingAndPositionWindow methodsFor: 'update' stamp: 'FHS 6/4/2023 17:11:34'!
updatePosition: aPosition
	record at: 1 put: aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeadingAndPositionWindow class' category: 'MarsRover-WithHeading'!
HeadingAndPositionWindow class
	instanceVariableNames: ''!

!HeadingAndPositionWindow class methodsFor: 'instance creation' stamp: 'FHS 6/4/2023 16:38:37'!
of: aMarsRover
	^self new initializeOf: aMarsRover.! !


!classDefinition: #HeadingLog category: 'MarsRover-WithHeading'!
Observers subclass: #HeadingLog
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadingLog methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:11:45'!
initializeOf: aMarsRover
	rover := aMarsRover.
	record := OrderedCollection new.
	
	aMarsRover addObserver: self.! !


!HeadingLog methodsFor: 'update' stamp: 'FHS 6/4/2023 17:11:45'!
updateDirection: aCardinalPoint
	record addLast: aCardinalPoint.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeadingLog class' category: 'MarsRover-WithHeading'!
HeadingLog class
	instanceVariableNames: ''!

!HeadingLog class methodsFor: 'instance creation' stamp: 'FHS 6/2/2023 15:39:50'!
of: aMarsRover
	^self new initializeOf: aMarsRover.! !


!classDefinition: #HeadingWindow category: 'MarsRover-WithHeading'!
Observers subclass: #HeadingWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadingWindow methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:11:56'!
initializeOf: aMarsRover
	rover := aMarsRover.
	record := #NotMovedYet.
	
	aMarsRover addObserver: self.! !


!HeadingWindow methodsFor: 'update' stamp: 'FHS 6/4/2023 17:11:56'!
updateDirection: aCardinalPoint
	record := ('Pointing ', aCardinalPoint).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HeadingWindow class' category: 'MarsRover-WithHeading'!
HeadingWindow class
	instanceVariableNames: ''!

!HeadingWindow class methodsFor: 'instance creation' stamp: 'FHS 6/4/2023 16:28:30'!
of: aMarsRover
	^self new initializeOf: aMarsRover.! !


!classDefinition: #PositionLog category: 'MarsRover-WithHeading'!
Observers subclass: #PositionLog
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!PositionLog methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:12:06'!
initializeOf: aMarsRover
	rover := aMarsRover.
	record := OrderedCollection new.
	
	aMarsRover addObserver: self.! !


!PositionLog methodsFor: 'update' stamp: 'FHS 6/4/2023 17:12:06'!
updatePosition: aPosition
	record addLast: aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PositionLog class' category: 'MarsRover-WithHeading'!
PositionLog class
	instanceVariableNames: ''!

!PositionLog class methodsFor: 'instance creation' stamp: 'FHS 6/2/2023 15:22:26'!
of: aMarsRover
	^self new initializeOf: aMarsRover.! !


!classDefinition: #PositionWindow category: 'MarsRover-WithHeading'!
Observers subclass: #PositionWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!PositionWindow methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:12:18'!
initializeOf: aMarsRover
	rover := aMarsRover.
	record := #NotMovedYet.
	
	aMarsRover addObserver: self.! !


!PositionWindow methodsFor: 'update' stamp: 'FHS 6/4/2023 17:12:18'!
updatePosition: aPosition
	record := aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PositionWindow class' category: 'MarsRover-WithHeading'!
PositionWindow class
	instanceVariableNames: 'position'!

!PositionWindow class methodsFor: 'instance creation' stamp: 'FHS 6/3/2023 11:33:28'!
of: aMarsRover
	^self new initializeOf: aMarsRover.! !


!classDefinition: #Updater category: 'MarsRover-WithHeading'!
Object subclass: #Updater
	instanceVariableNames: 'observers headingToCardinalPointDictionary'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Updater methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:04:34'!
createCardinalPointDictionary

	headingToCardinalPointDictionary := Dictionary new.
	headingToCardinalPointDictionary at: MarsRoverHeadingEast put: 'East'.
	headingToCardinalPointDictionary at: MarsRoverHeadingNorth put: 'North'.
	headingToCardinalPointDictionary at: MarsRoverHeadingWest put: 'West'.
	headingToCardinalPointDictionary at: MarsRoverHeadingSouth put: 'South'! !

!Updater methodsFor: 'initialization' stamp: 'FHS 6/4/2023 17:04:34'!
initialize
	observers := OrderedCollection new.
	self createCardinalPointDictionary.! !


!Updater methodsFor: 'observer' stamp: 'FHS 6/3/2023 12:05:23'!
addObserver: anObserver
	observers addLast: anObserver.! !


!Updater methodsFor: 'update' stamp: 'FHS 6/4/2023 17:05:31'!
updateDirection: aDirection
	|cardinalDirection|
	cardinalDirection := headingToCardinalPointDictionary at: aDirection class ifAbsent:[self error: self class notAValidHeadingDirectionErrorDescription].
	
	observers do: [:anObserver |
		anObserver updateDirection: cardinalDirection.
		].! !

!Updater methodsFor: 'update' stamp: 'FHS 6/3/2023 12:11:51'!
updatePosition: aPosition
	observers do: [:anObserver |
		anObserver updatePosition: aPosition.
		].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Updater class' category: 'MarsRover-WithHeading'!
Updater class
	instanceVariableNames: ''!

!Updater class methodsFor: 'error description' stamp: 'FHS 6/4/2023 17:05:40'!
notAValidHeadingDirectionErrorDescription

	^ 'Not a valid heading direction'! !
!classDefinition: #MarsRoverLogTest category: 'MarsRover-Tests'!
TestCase subclass: #MarsRoverLogTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Tests'!

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 15:39:17'!
test01newMarsRoverHasAnEmptyPositionLog
	| marsRover positionLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	positionLog := PositionLog of: marsRover.
	
	self assert: positionLog show isEmpty ! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 15:39:08'!
test02marsRoverHasFowardMovementLogged
	| marsRover positionLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	positionLog := PositionLog of: marsRover.
	
	marsRover process: 'f'.
	
	self assert: (OrderedCollection with: 1@2) equals: positionLog show.! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 15:39:01'!
test03marsRoverHasBackwardMovementLogged
	| marsRover positionLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	positionLog := PositionLog of: marsRover.
	
	marsRover process: 'b'.
	
	self assert: (OrderedCollection with: 1@0) equals: positionLog show.! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 15:39:29'!
test04newMarsRoverHasAnEmptyHeadingLog
	| marsRover headingLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingLog := HeadingLog of: marsRover.
	
	self assert: headingLog show isEmpty 
	
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 15:59:38'!
test05marsRoverThatStartsHeadingNorthAndGoesRightHasHeadingEastLogged
	| marsRover headingLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingLog := HeadingLog of: marsRover.
	
	marsRover process: 'r'.
	
	self assert: (OrderedCollection with: 'East') equals: headingLog show. 
	
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 16:00:39'!
test06marsRoverThatStartsHeadingNorthAndGoesLeftHasHeadingWestLogged
	| marsRover headingLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingLog := HeadingLog of: marsRover.
	
	marsRover process: 'l'.
	
	self assert: (OrderedCollection with: 'West') equals: headingLog show. 
	
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 16:01:44'!
test07marsRoverThatStartsHeadingWestAndGoesLeftHasHeadingSouthLogged
	| marsRover headingLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingWest. 
	headingLog := HeadingLog of: marsRover.
	
	marsRover process: 'l'.
	
	self assert: (OrderedCollection with: 'South') equals: headingLog show. 
	
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/2/2023 16:02:11'!
test08marsRoverThatStartsHeadingWestAndGoesRightHasHeadingNorthLogged
	| marsRover headingLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingWest. 
	headingLog := HeadingLog of: marsRover.
	
	marsRover process: 'r'.
	
	self assert: (OrderedCollection with: 'North') equals: headingLog show. 
	
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/3/2023 12:04:21'!
test09marsRoverCanHaveMultipleLogs
	| marsRover headingLog positionLog|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingLog := HeadingLog of: marsRover.
	positionLog := PositionLog of: marsRover.
	
	marsRover process: 'fr'.
	
	self assert: (OrderedCollection with: 1@2) equals: positionLog show. 
	self assert: (OrderedCollection with: 'East') equals: headingLog show. 
! !

!MarsRoverLogTest methodsFor: 'tests' stamp: 'FHS 6/4/2023 15:59:55'!
test10marsRoverCanHavePositionAndHeadingLoggedOnSameLog
	| marsRover headingAndPositionLog |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingAndPositionLog := HeadingAndPositionLog of: marsRover.
	
	marsRover process: 'fr'.
	
	self assert: (OrderedCollection with: 1@2 with: 'East') equals: headingAndPositionLog show. 
	"Habiendo hecho el log de position y de heading damos un salto mas grande para este test"! !


!classDefinition: #MarsRoverTest category: 'MarsRover-Tests'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Tests'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #MarsRoverWindowTest category: 'MarsRover-Tests'!
TestCase subclass: #MarsRoverWindowTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Tests'!

!MarsRoverWindowTest methodsFor: 'tests' stamp: 'FHS 6/4/2023 17:26:58'!
test01newMarsRoverHasAnEmptyPositionWindow
	| marsRover positionWindow|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	positionWindow := PositionWindow of: marsRover.
	
	self assert: #NotMovedYet equals: positionWindow show. 
! !

!MarsRoverWindowTest methodsFor: 'tests' stamp: 'FHS 6/4/2023 16:26:01'!
test02marsRoverShowsPositionChange
	| marsRover positionWindow|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	positionWindow := PositionWindow of: marsRover.
	marsRover process: 'f'.
	
	self assert: 1@2 equals: positionWindow show.
! !

!MarsRoverWindowTest methodsFor: 'tests' stamp: 'FHS 6/4/2023 16:27:11'!
test03newMarsRoverHasAnEmptyHeadingWindow
	| marsRover headingWindow|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingWindow := HeadingWindow of: marsRover.
	
	self assert: #NotMovedYet equals: headingWindow show. 
! !

!MarsRoverWindowTest methodsFor: 'tests' stamp: 'FHS 6/4/2023 16:29:29'!
test04marsRoverShowsDirectionChange
	| marsRover headingWindow|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingWindow := HeadingWindow of: marsRover.
	marsRover process: 'r'.
	
	self assert: 'Pointing East' equals: headingWindow show.
	
	"Evitamos hacer un test para aceptar cada cambio de punto cardinal
	puesto que ya fue testeado ya en los Logs."
! !

!MarsRoverWindowTest methodsFor: 'tests' stamp: 'FHS 6/4/2023 16:38:49'!
test05marsRoverWindowShowsPositionAndHeadingChanges
	| marsRover headingAndPositionWindow|
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth. 
	headingAndPositionWindow := HeadingAndPositionWindow of: marsRover.
	marsRover process: 'fr'.
	
	self assert: (OrderedCollection with: 1@2 with: 'Pointing East') 
		equals: headingAndPositionWindow show.
	
! !
