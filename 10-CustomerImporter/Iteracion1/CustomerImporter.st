!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'select' stamp: 'FHS 6/23/2023 15:28:33'!
selectPersonWithIdType: idType andIdNumber: idNumber 
	| customer |
	customer  := session select: [:aCustomer | aCustomer identificationType = idType 
									and: [aCustomer identificationNumber = idNumber]]
					 ofType: Customer.
					
	self assert: 1 equals: customer 		size. "Nos aseguramos que solo hay una persona con esa ID"
	
	"anyOne lo hereda de Collection y te da cualquier item 
	(en este caso como solo hay uno obtenemos ese. Util para no tener que pasarlo a una ordered collection)"
	^customer anyOne.! !


!ImportTest methodsFor: 'test' stamp: 'FHS 6/24/2023 16:32:50'!
test01Import

	self importCustomersFromDefaultStream.
	
	self assertThereAreOnlyTwoCustomers.
	self assertCustomerIsPepeSanchez: (self selectPersonWithIdType: 'D' andIdNumber: '22333444').
	self assertCustomerIsJuanPerez: (self selectPersonWithIdType: 'C'  andIdNumber: '23-25666777-9').
	! !


!ImportTest methodsFor: 'assertions' stamp: 'FHS 6/23/2023 15:32:25'!
assertCustomerIsJuanPerez: aCustomerToBeVerified
	| juanPerez |
	juanPerez := Customer name: 'Juan' 
					lastname: 	'Perez' 
					idType: 'C'
					idNumber: '23-25666777-9'	.
	
	juanPerez addAddress: (Address on: 'Alem'
									at: 1122
									inTown: 'CABA'
									inProvince: 'CABA'
									withZipCode: 1001). 
	
	self assert: aCustomerToBeVerified = juanPerez.
	! !

!ImportTest methodsFor: 'assertions' stamp: 'FHS 6/23/2023 15:04:57'!
assertCustomerIsPepeSanchez: aCustomerToBeVerified
	| pepeSanchez |
	pepeSanchez := Customer name: 'Pepe' 
					lastname: 	'Sanchez' 
					idType: 'D'
					idNumber: '22333444'	.
	
	pepeSanchez addAddress: (Address on: 'San Martin'
									at: 3322
									inTown: 'Olivos'
									inProvince: 'BsAs'
									withZipCode: 1636). 
						
	pepeSanchez addAddress: (Address on: 'Maipu'
									at: 888
									inTown: 'Florida'
									inProvince: 'Buenos Aires'
									withZipCode: 1122). 
	 
	
	self assert: aCustomerToBeVerified = pepeSanchez.
	! !

!ImportTest methodsFor: 'assertions' stamp: 'FHS 6/24/2023 16:16:23'!
assertThereAreOnlyTwoCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !


!ImportTest methodsFor: 'test support - private' stamp: 'FHS 6/23/2023 14:17:35'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction! !

!ImportTest methodsFor: 'test support - private' stamp: 'FHS 6/23/2023 14:17:53'!
tearDown

	session commit.
	session close! !


!ImportTest methodsFor: 'import' stamp: 'FHS 6/24/2023 16:33:49'!
defaultStream
^ ReadStream on: 
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'
"Lo dejamos contra la izquierda puesto que de separarlo se detectan como espacios y el test falla."! !

!ImportTest methodsFor: 'import' stamp: 'FHS 6/24/2023 16:33:35'!
importCustomersFromDefaultStream
	^ (CustomerImporter onSession: session fromStream: self defaultStream ) import! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'town' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'town' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!Address methodsFor: 'initialization' stamp: 'FHS 6/23/2023 15:14:47'!
initializeOn: aStreetName at: aStreetNumber inTown: aTownName inProvince: aProvinceName withZipCode: aZipCode 
	
	streetName := aStreetName asUnicodeString.
	streetNumber := aStreetNumber.
	town := aTownName asUnicodeString.
	province := aProvinceName asUnicodeString.
	zipCode := aZipCode.! !


!Address methodsFor: 'comparing' stamp: 'FHS 6/23/2023 15:20:08'!
= anAddress
		^anAddress streetName = streetName and: [
			anAddress streetNumber = streetNumber and: [
				anAddress town = town and: [
						anAddress province = province and: [
							anAddress zipCode = zipCode .
						]
					]
				]
			]! !

!Address methodsFor: 'comparing' stamp: 'FHS 6/23/2023 15:38:33'!
hash
	^ streetName hash bitXor: (
		streetNumber hash bitXor: (
			town hash bitXor: (
				province hash bitXor: (
					zipCode hash)
					))).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Address class' category: 'CustomerImporter'!
Address class
	instanceVariableNames: ''!

!Address class methodsFor: 'instance creation' stamp: 'FHS 6/23/2023 14:58:25'!
on: aStreetName at: aStreetNumber inTown: aTownName inProvince: aProvinceName withZipCode: aZipCode 
	^self new initializeOn: aStreetName at: aStreetNumber inTown: aTownName inProvince: aProvinceName withZipCode: aZipCode ! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !

!Customer methodsFor: 'initialization' stamp: 'FHS 6/25/2023 10:19:51'!
initializeName: aCustomerName lastname: aCustomerLastName idType: idType idNumber: idNumber 

	firstName := aCustomerName asUnicodeString.
	lastName := aCustomerLastName asUnicodeString.
	identificationType := idType asUnicodeString.
	identificationNumber := idNumber asUnicodeString.
	
	"Tal como nos dijo Hernán, no debemos las cosas del initialize aquí.
	Por lo tanto el super initialize y la asignacion de addresses se realizan en el initialize"
	! !


!Customer methodsFor: 'comparing' stamp: 'FHS 6/23/2023 14:42:47'!
= aCustomer

	^aCustomer firstName = firstName and: [
			aCustomer lastName = lastName and: [
				aCustomer identificationType = identificationType and: [
						aCustomer identificationNumber = identificationNumber and: [
							aCustomer addresses = addresses.
						]
					]
				]
			]! !

!Customer methodsFor: 'comparing' stamp: 'FHS 6/23/2023 15:38:40'!
hash
	^ firstName hash bitXor: (
		lastName hash bitXor: (
			identificationType hash bitXor: (
				identificationNumber hash bitXor: (
					addresses hash)
					))).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Customer class' category: 'CustomerImporter'!
Customer class
	instanceVariableNames: ''!


!Customer class methodsFor: 'instance creation' stamp: 'FHS 6/23/2023 14:49:56'!
name: aCustomerName lastname: aCustomerLastName idType: identificationType idNumber: identificationNumber 
	^self new initializeName: aCustomerName lastname: aCustomerLastName idType: identificationType idNumber: identificationNumber ! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readableStream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'FHS 6/24/2023 16:25:51'!
initializeOnSession: aSession fromStream: aReadableStream 
	session := aSession.
	readableStream := aReadableStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'FHS 6/24/2023 16:26:55'!
import
	| newCustomer line |

	line := readableStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth.
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth ].

		line := readableStream nextLine. ].

	readableStream close.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'FHS 6/24/2023 16:25:34'!
onSession: aSession fromStream: aReadableStream 
	^self new initializeOnSession: aSession fromStream: aReadableStream! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
