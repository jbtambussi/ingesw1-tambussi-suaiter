!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 10:31:10'!
test01importFromValidFile

	CustomerImporter valueFrom: self validImportData into: session.

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:09:21'!
test02cannotReadAnEmptyFile
"Para el corrector: Segun lo consultado en clase nos dijeron que debemos ser fieles al enunciado
 y mantener el nombre de que estamos operando sobre un archivo (file). De ahí los nombres de los tests"
	self 
		assertThatFile: (ReadStream on: '')	 
		raisesError: CustomerImporter cannotReadAnEmptyFileErrorDescription 
		andManteinsInvariant: [self assert: session isEmpty.].

! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:15:19'!
test03cannotReadAFileWithACustomerWithoutTheNecessaryFields

	self 
		assertThatFile: self invalidImportDataWithCustomerWithoutIDNumber 
		raisesError: CustomerImporter cannotReadAFileWithoutTheNecessaryFieldsErrorDescription 
		andManteinsInvariant: [self assert: session isEmpty.].

! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:15:01'!
test04cannotReadAFileWithAnAddressWithoutTheNecessaryFields

	self 
		assertThatFile: self invalidImportDataWithAddressWithoutStreetNumber 
		raisesError: CustomerImporter cannotReadAFileWithoutTheNecessaryFieldsErrorDescription 
		andManteinsInvariant: [self assertThereIsOneCustomerAndZeroAddresses.].
	
		

! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:15:27'!
test05cannotReadAFileWithALineStartingWithAnInvalidCharacter

	self 
		assertThatFile: self invalidImportDataWithInvalidStartingCharacter 
		raisesError: CustomerImporter cannotReadAFileWithALineStartingWithAnInvalidCharacterErrorDescription 
		andManteinsInvariant: [self assert: session isEmpty.].
		

! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:14:51'!
test06cannotReadAFileWithAnAddressBeforeAnyCustomer

	self 
		assertThatFile: self invalidImportDataWithAddressWithoutACustomerBefore 
		raisesError: CustomerImporter cannotReadAFileWithAnAddressBeforeAnyCustomerErrorDescription
		andManteinsInvariant: [self assert: session isEmpty].
		

! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:15:11'!
test07cannotReadAFileWithAnEmptyLine
	"Este caso ya fue testeado pero lo dejamos por completitud ya que se cubre con el chequeo del caracter inválido"
	self 
		assertThatFile: self invalidImportDataWithAnEmptyLine 
		raisesError: CustomerImporter cannotReadAFileWithALineStartingWithAnInvalidCharacterErrorDescription 
		andManteinsInvariant: [self assertThereIsOneCustomerAndZeroAddresses.].
		

! !

!ImportTest methodsFor: 'tests' stamp: 'FHS 6/29/2023 11:35:52'!
test08cannotReadAFileWithMoreFieldsThanExpected
	" Al corrector: Este test nos ayudó a darnos cuenta de que estabamos detectando mal las comas en un comiezo (creaimos haberlo hecho bien)"
	self 
		assertThatFile: (ReadStream on: 'C, , , , ,')
		raisesError: CustomerImporter cannotReadAFileWithoutTheNecessaryFieldsErrorDescription
		andManteinsInvariant: [self assert: session isEmpty.].
		

! !


!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:27:57'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := self customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:12:18'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !

!ImportTest methodsFor: 'assertions' stamp: 'FHS 6/29/2023 11:05:23'!
assertThatFile: aFile raisesError: anErrorToVerify andManteinsInvariant: anInvariantBlock

	self 
		should: [CustomerImporter valueFrom: aFile		into: session.]
		raise: Error
		withExceptionDo: 
		[:anError | 
			self assert: anErrorToVerify equals: anError messageText.
			anInvariantBlock value.
		].! !

!ImportTest methodsFor: 'assertions' stamp: 'FHS 6/29/2023 10:57:25'!
assertThereIsOneCustomerAndZeroAddresses

	self assert: (session selectAllOfType: Customer) size = 1.
			self assert: (session selectAllOfType: Address) size =0! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:27:50'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !


!ImportTest methodsFor: 'customer' stamp: 'HAW 5/22/2022 18:14:22'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ (session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !


!ImportTest methodsFor: 'test data - valid' stamp: 'HAW 5/22/2022 18:08:08'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!ImportTest methodsFor: 'test data - invalid' stamp: 'FHS 6/29/2023 11:14:51'!
invalidImportDataWithAddressWithoutACustomerBefore

	^ ReadStream on: 'A,San Martin,3322,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'test data - invalid' stamp: 'FHS 6/29/2023 11:15:01'!
invalidImportDataWithAddressWithoutStreetNumber

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444
A,San Martin,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'test data - invalid' stamp: 'FHS 6/29/2023 11:15:10'!
invalidImportDataWithAnEmptyLine

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444

A,San Martin,3322,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'test data - invalid' stamp: 'FHS 6/29/2023 11:15:19'!
invalidImportDataWithCustomerWithoutIDNumber

	^ ReadStream on: 'C,Pepe,Sanchez,D'! !

!ImportTest methodsFor: 'test data - invalid' stamp: 'FHS 6/29/2023 11:15:27'!
invalidImportDataWithInvalidStartingCharacter

	^ ReadStream on: 'K,Pepe,Sanchez,D,22333444'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session file actualCustomer data record line newCustomer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'FHS 6/29/2023 11:37:43'!
initializeFrom: aFile into: aSession
	session := aSession.
	file := aFile.
	"Para el corrector: Segun lo consultado en clase nos dijeron que debemos ser fieles al enunciado
 y mantener el nombre de que estamos operando sobre un archivo (file). De ahí que se haga referencia a un file y no a un stream"! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'FHS 6/26/2023 20:47:09'!
value
	[self hasLinesLeft ] whileTrue: [
		self readRecord.
		self importRecord.
	].

	! !


!CustomerImporter methodsFor: 'testing' stamp: 'FHS 6/26/2023 20:13:20'!
isAddressRecord

	^ line beginsWith: 'A'! !

!CustomerImporter methodsFor: 'testing' stamp: 'FHS 6/26/2023 20:13:02'!
isCustomerRecord

	^ line beginsWith: 'C'! !


!CustomerImporter methodsFor: 'read record' stamp: 'FHS 6/29/2023 11:36:57'!
hasLinesLeft

	line := file nextLine. 
	^line notNil! !

!CustomerImporter methodsFor: 'read record' stamp: 'FHS 6/26/2023 21:02:27'!
readRecord

	^ record := line findTokens: $,.! !


!CustomerImporter methodsFor: 'import' stamp: 'FHS 6/29/2023 10:34:40'!
importAddress
	|newAddress|
	
	self assertThereIsACustomerBeforeTheAddress.
	self assertAmountOfFieldsEquals: 6.
	
	newAddress := Address new.
	newCustomer addAddress: newAddress.
	newAddress streetName: record second.
	newAddress streetNumber: record third asNumber .
	newAddress town: record fourth.
	newAddress zipCode: record fifth asNumber .
	newAddress province: record sixth! !

!CustomerImporter methodsFor: 'import' stamp: 'FHS 6/29/2023 10:35:53'!
importCustomer
	
	self assertAmountOfFieldsEquals: 5.
	
	newCustomer := Customer new.
			newCustomer firstName: record second.
			newCustomer lastName: record third.
			newCustomer identificationType: record fourth.
			newCustomer identificationNumber: record fifth.
			session persist: newCustomer! !

!CustomerImporter methodsFor: 'import' stamp: 'FHS 6/29/2023 10:37:51'!
importRecord
	
	self isCustomerRecord ifTrue: [^self importCustomer ].
	self isAddressRecord ifTrue: [^self importAddress ].
	
	" Si llega aca es porque no fue ni C ni A. De esta forma nos ahorramos el if "
	self error: self class cannotReadAFileWithALineStartingWithAnInvalidCharacterErrorDescription.
	! !


!CustomerImporter methodsFor: 'assertions' stamp: 'FHS 6/29/2023 11:31:41'!
assertAmountOfFieldsEquals: anAmount
	|expectedAmountOfCommas|
	expectedAmountOfCommas := anAmount -1.
	
	"Al corrector: Decidimos verificar por cantidad de comas puesto de que la manera anterior
	que realizabamos podíamos tener una coma extra al final sin detectar el error."
	
	^ ((line occurrencesOf: $,) = expectedAmountOfCommas)
	ifFalse: [self error: self class cannotReadAFileWithoutTheNecessaryFieldsErrorDescription ]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'FHS 6/29/2023 10:39:28'!
assertThereIsACustomerBeforeTheAddress

	^ newCustomer ifNil: [self error: self class cannotReadAFileWithAnAddressBeforeAnyCustomerErrorDescription ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'assertions' stamp: 'FHS 6/29/2023 10:32:34'!
assertFileIsNotEmpty: aFile

	^ aFile isEmpty ifTrue: [self error: self cannotReadAnEmptyFileErrorDescription ]! !


!CustomerImporter class methodsFor: 'importing' stamp: 'FHS 6/29/2023 10:32:25'!
valueFrom: aFile into: aSession
	self assertFileIsNotEmpty: aFile.
	^(self from: aFile into: aSession) value! !


!CustomerImporter class methodsFor: 'error descriptions' stamp: 'FHS 6/29/2023 10:37:51'!
cannotReadAFileWithALineStartingWithAnInvalidCharacterErrorDescription
	^'Cant read a stream with a line starting with an invalid character'! !

!CustomerImporter class methodsFor: 'error descriptions' stamp: 'FHS 6/29/2023 10:39:28'!
cannotReadAFileWithAnAddressBeforeAnyCustomerErrorDescription
	^'Cant read a stream with an address before any customer'! !

!CustomerImporter class methodsFor: 'error descriptions' stamp: 'FHS 6/26/2023 21:46:22'!
cannotReadAFileWithAnEmptyLineErrorDescription
	^'Cant read a file with an empty line'! !

!CustomerImporter class methodsFor: 'error descriptions' stamp: 'FHS 6/29/2023 10:35:06'!
cannotReadAFileWithoutTheNecessaryFieldsErrorDescription
	^'Cant read a file that has a line without the necessary fields'! !

!CustomerImporter class methodsFor: 'error descriptions' stamp: 'FHS 6/29/2023 10:31:35'!
cannotReadAnEmptyFileErrorDescription
	^'Cant read from an empty file'! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !


!DataBaseSession methodsFor: 'testing' stamp: 'FHS 6/26/2023 20:53:57'!
isEmpty
	^tables isEmpty ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
