!classDefinition: #DrivingAssistantTest category: 'ISW1-2023-1C-2doParcial'!
TestCase subclass: #DrivingAssistantTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test01DrivingAssitantStartsInManualWithCorrespondingValues
	|drivingAssistant driveSystem proximitySensor speedSensor |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
			withProximitySensor: proximitySensor 
			withDriveSystem: driveSystem.
	
	self assert: 0*(kilometer/hour) equals: drivingAssistant speed.
	self assert: #OFF equals: driveSystem proximityBeepStatus.
	self assert: #CONNECTED equals: driveSystem throttleStatus.
	self assert: #RELEASED equals: driveSystem brakeStatus.
	self assert: nil equals: driveSystem targetSpeed.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test02DrivingAssitantStartsInAutomaticWithCorrespondingValues
	|drivingAssistant driveSystem proximitySensor speedSensor |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: 100*kilometer/hour .
		
	drivingAssistant := DrivingAssistant startWithDrivingModeAutomaticWithSpeedSensor: speedSensor 
		withProximitySensor: proximitySensor 
		withDriveSystem: driveSystem.
	
	self assert: 0*(kilometer/hour) equals: drivingAssistant speed.
	self assert: #OFF equals: driveSystem proximityBeepStatus.
	self assert: #CONNECTED equals: driveSystem throttleStatus.
	self assert: #RELEASED equals: driveSystem brakeStatus.
	self assert: 100*(kilometer /hour) equals: driveSystem targetSpeed.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test03DrivingAssitantStartsInManualAssistedWithCorrespondingValues
	|drivingAssistant driveSystem proximitySensor speedSensor |
	
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: 100*kilometer/hour .
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualAssistedWithSpeedSensor: speedSensor 
		withProximitySensor: proximitySensor 
		withDriveSystem: driveSystem.
	
	self assert: 0*(kilometer/hour) equals: drivingAssistant speed.
	self assert: #OFF equals: driveSystem proximityBeepStatus.
	self assert: #CONNECTED equals: driveSystem throttleStatus.
	self assert: #RELEASED equals: driveSystem brakeStatus.
	self assert: 100*(kilometer /hour) equals: driveSystem targetSpeed.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test04SpeedSensorReturnsActualSpeed
	|drivingAssistant proximitySensor speedSensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).		
	driveSystem := DriveSystem withTargetSpeed: 100*kilometer/hour .
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
			withProximitySensor: proximitySensor 
			withDriveSystem: driveSystem.
	
	drivingAssistant tick.
	
	self assert: 100*kilometer/hour equals: drivingAssistant speed.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test05FrontProximitySensorReturnsDistanceToAnObject

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
			withProximitySensor: proximitySensor 
			withDriveSystem: driveSystem.
	
	drivingAssistant tick.
	
	self assert: 25*meter equals: drivingAssistant distanceToCurrentObject.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test06DriveAssistantCanChangeDriveSystemBeepStatusToDanger

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant dangerProximityBeep.
	
	self assert: #DANGER equals: driveSystem proximityBeepStatus.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test07DriveAssistantCanChangeDriveSystemBeepStatusToWarning

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant warningProximityBeep.
	
	self assert: #WARNING equals: driveSystem proximityBeepStatus.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test08DriveAssistantCanChangeDriveSystemBeepStatusToOff

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant warningProximityBeep.
	drivingAssistant turnOffProximityBeep.
	
	self assert: #OFF equals: driveSystem proximityBeepStatus.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test09DriveAssistantCanChangeDriveSystemThrottleStatusToDisconnected

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant disconnectThrottle.
	
	self assert: #DISCONNECTED equals: driveSystem throttleStatus.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test10DriveAssistantCanChangeDriveSystemThrottleStatusToConnected

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant disconnectThrottle.
	drivingAssistant connectThrottle.
	
	self assert: #CONNECTED equals: driveSystem throttleStatus.
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test11DriveAssistantCanChangeDriveSystemBreakStatusToApplied

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant applyBrakes.
	
	self assert: #APPLIED equals: driveSystem brakeStatus .
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test12DriveAssistantCanChangeDriveSystemBreakStatusToReleased

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant applyBrakes.
	drivingAssistant releaseBrakes.
	
	self assert: #RELEASED equals: driveSystem brakeStatus .
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test13DriveAssistantCanChangeDriveSystemToKeepASpeed

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant keepSpeedAt: 50*kilometer/hour.
	
	self assert: 50*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:01:26'!
test14DriveAssistantCanCalculateDMF

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {25*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: (100*kilometer/hour)^2 / (180000*kilometer/(hour^2)) equals: drivingAssistant actualDMF
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:24:37'!
test15DriveAssistantCanTellIfDistanceZoneIsSafe

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {10*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {250000*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: (10*kilometer/hour)^2 / (180000*kilometer/(hour^2)) equals: drivingAssistant actualDMF.
	self assert: (drivingAssistant calculateZoneDistance isKindOf: SafeZone ).
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:24:37'!
test16DriveAssistantCanTellIfDistanceZoneIsWarning

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {65*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: (100*kilometer/hour)^2 / (180000*kilometer/(hour^2)) equals: drivingAssistant actualDMF.
	self assert: (drivingAssistant calculateZoneDistance isKindOf: WarningZone).
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:24:37'!
test17DriveAssistantCanTellIfDistanceZoneIsDanger

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {1000*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {6*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: (1000*kilometer/hour)^2 / (180000*kilometer/(hour^2)) equals: drivingAssistant actualDMF.
	self assert: (drivingAssistant calculateZoneDistance isKindOf: DangerZone ).
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:24:37'!
test18DriveAssistantCanTellIfDistanceZoneIsSafeBecauseThereIsntAnyObject

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {nil}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: (100*kilometer/hour)^2 / (180000*kilometer/(hour^2)) equals: drivingAssistant actualDMF.
	self assert: (drivingAssistant calculateZoneDistance isKindOf: SafeZone).
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:08:44'!
test19IfDriveAssistantOnManualSensesDangerZoneItPutsProximityBeepOnDanger

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {1000*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {6*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: #DANGER equals: driveSystem proximityBeepStatus.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:10:38'!
test20IfDriveAssistantOnManualSensesWarningZoneItPutsProximityBeepOnWarning

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {65*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: #WARNING equals: driveSystem proximityBeepStatus.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:10:34'!
test21IfDriveAssistantOnManualSensesSafeZoneItTurnsOffProximityBeep

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {10*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {250000*meter}).
	driveSystem := DriveSystem withTargetSpeed: nil.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualWithSpeedSensor: speedSensor 
									withProximitySensor: proximitySensor 
									withDriveSystem: driveSystem.
									
	drivingAssistant tick.
	
	self assert: #OFF equals: driveSystem proximityBeepStatus.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:38:05'!
test22DriveAssistantOnAutomaticSensesDangerZone

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {1000*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {6*meter}).
	driveSystem := DriveSystem withTargetSpeed: 20*kilometer/hour.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeAutomaticWithSpeedSensor: speedSensor 
						withProximitySensor: proximitySensor 
						withDriveSystem: driveSystem.
	drivingAssistant tick.
	
	self assert: #APPLIED equals: driveSystem brakeStatus.
	self assert: #DISCONNECTED equals: driveSystem throttleStatus.
	self assert: 10*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:38:47'!
test23DriveAssistantOnAutomaticSensesWarningZone

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {65*meter}).
	driveSystem := DriveSystem withTargetSpeed: 20*kilometer/hour.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeAutomaticWithSpeedSensor: speedSensor 
						withProximitySensor: proximitySensor 
						withDriveSystem: driveSystem.
	drivingAssistant tick.
	
	self assert: #RELEASED equals: driveSystem brakeStatus.
	self assert: #DISCONNECTED equals: driveSystem throttleStatus.
	self assert: 15*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:40:23'!
test24DriveAssistantOnAutomaticSensesSafeZone

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {10*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {250000*meter}).
	driveSystem := DriveSystem withTargetSpeed: 20*kilometer/hour.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeAutomaticWithSpeedSensor: speedSensor 
						withProximitySensor: proximitySensor 
						withDriveSystem: driveSystem.
	drivingAssistant tick.
	
	self assert: 20*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:43:21'!
test25DriveAssistantOnManualAssistedSensesDangerZone

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {1000*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {6*meter}).
	driveSystem := DriveSystem withTargetSpeed: 20*kilometer/hour.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualAssistedWithSpeedSensor: speedSensor
						withProximitySensor: proximitySensor 
						withDriveSystem: driveSystem. 
						
	drivingAssistant tick.
	
	self assert: #DANGER equals: driveSystem proximityBeepStatus.
	self assert: #APPLIED equals: driveSystem brakeStatus.
	self assert: #DISCONNECTED equals: driveSystem throttleStatus.
	self assert: 10*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:46:25'!
test26DriveAssistantOnManualAssistedSensesWarningZone
	"Lo dejo por completitud"

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: ( ReadStream on: {100*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {65*meter}).
	driveSystem := DriveSystem withTargetSpeed: 20*kilometer/hour.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualAssistedWithSpeedSensor: speedSensor
						withProximitySensor: proximitySensor 
						withDriveSystem: driveSystem. 
	drivingAssistant tick.
	
	self assert: #WARNING equals: driveSystem proximityBeepStatus.
	self assert: #RELEASED equals: driveSystem brakeStatus.
	self assert: #DISCONNECTED equals: driveSystem throttleStatus.
	self assert: 15*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !

!DrivingAssistantTest methodsFor: 'tests' stamp: 'FHS 7/6/2023 21:47:07'!
test27DriveAssistantOnManualAssistedSensesSafeZone
	"Lo dejo por completitud"

	|drivingAssistant speedSensor proximitySensor driveSystem |
	speedSensor := SpeedSensor withEvents: (ReadStream on: {10*kilometer/hour}).
	proximitySensor := ProximitySensor withEvents: (ReadStream on: {250000*meter}).
	driveSystem := DriveSystem withTargetSpeed: 20*kilometer/hour.
	
	drivingAssistant := DrivingAssistant startWithDrivingModeManualAssistedWithSpeedSensor: speedSensor
						withProximitySensor: proximitySensor 
						withDriveSystem: driveSystem. 
	drivingAssistant tick.
	
	self assert: #OFF equals: driveSystem proximityBeepStatus.
	self assert: 20*kilometer/hour equals: driveSystem targetSpeed.
	
	
	
	! !


!classDefinition: #DistanceZone category: 'ISW1-2023-1C-2doParcial'!
Object subclass: #DistanceZone
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!DistanceZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:34:18'!
manualBehaviorWith: aDrivingAssistant
	self subclassResponsibility ! !


!classDefinition: #DangerZone category: 'ISW1-2023-1C-2doParcial'!
DistanceZone subclass: #DangerZone
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!DangerZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:41:32'!
automaticBehaviorWith: anAutomaticDrivingAssistant
	anAutomaticDrivingAssistant applyBrakes.
	anAutomaticDrivingAssistant disconnectThrottle.
	anAutomaticDrivingAssistant reduceTargetSpeedBy: 10*kilometer/hour
	
! !

!DangerZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:39:30'!
manualBehaviorWith: aManualDrivingAssistant
	aManualDrivingAssistant dangerProximityBeep.
! !


!classDefinition: #SafeZone category: 'ISW1-2023-1C-2doParcial'!
DistanceZone subclass: #SafeZone
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!SafeZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:40:44'!
automaticBehaviorWith: anAutomaticDrivingAssistant
	"No hace falta modificar nada"! !

!SafeZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:39:26'!
manualBehaviorWith: aManualDrivingAssistant
	aManualDrivingAssistant turnOffProximityBeep.
! !


!classDefinition: #WarningZone category: 'ISW1-2023-1C-2doParcial'!
DistanceZone subclass: #WarningZone
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!WarningZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:41:32'!
automaticBehaviorWith: anAutomaticDrivingAssistant
	anAutomaticDrivingAssistant releaseBrakes .
	anAutomaticDrivingAssistant disconnectThrottle.
	anAutomaticDrivingAssistant reduceTargetSpeedBy: 5*kilometer/hour! !

!WarningZone methodsFor: 'zone behavior' stamp: 'FHS 7/6/2023 21:39:20'!
manualBehaviorWith: aManualDrivingAssistant
	aManualDrivingAssistant warningProximityBeep.
! !


!classDefinition: #DriveSystem category: 'ISW1-2023-1C-2doParcial'!
Object subclass: #DriveSystem
	instanceVariableNames: 'proximityBeepStatus throttleStatus brakeStatus speed targetSpeed'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!DriveSystem methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:51:28'!
dangerProximityBeep
	proximityBeepStatus := #DANGER! !

!DriveSystem methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:51:17'!
proximityBeepStatus
	^proximityBeepStatus ! !

!DriveSystem methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:54:11'!
turnOffProximityBeep
	proximityBeepStatus := #OFF.! !

!DriveSystem methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:52:53'!
warningProximityBeep
	proximityBeepStatus := #WARNING! !


!DriveSystem methodsFor: 'throttle' stamp: 'FHS 7/6/2023 19:58:14'!
connectThrottle
	throttleStatus := #CONNECTED.! !

!DriveSystem methodsFor: 'throttle' stamp: 'FHS 7/6/2023 19:57:04'!
disconnectThrottle
	throttleStatus := #DISCONNECTED.! !

!DriveSystem methodsFor: 'throttle' stamp: 'FHS 7/6/2023 19:41:22'!
throttleStatus
	^throttleStatus ! !


!DriveSystem methodsFor: 'initialization' stamp: 'FHS 7/6/2023 20:21:59'!
initializeWithTargetSpeed: aTargetSpeed 
	proximityBeepStatus := #OFF.
	throttleStatus := #CONNECTED.
	brakeStatus := #RELEASED.
	targetSpeed  := aTargetSpeed! !


!DriveSystem methodsFor: 'breakes' stamp: 'FHS 7/6/2023 20:01:58'!
applyBrakes
	brakeStatus := #APPLIED! !

!DriveSystem methodsFor: 'breakes' stamp: 'FHS 7/6/2023 19:40:54'!
brakeStatus
	^brakeStatus ! !

!DriveSystem methodsFor: 'breakes' stamp: 'FHS 7/6/2023 20:04:37'!
releaseBrakes
	brakeStatus := #RELEASED! !


!DriveSystem methodsFor: 'target speed' stamp: 'FHS 7/6/2023 20:27:52'!
keepSpeedAt: aTargetSpeed 
	targetSpeed := aTargetSpeed.! !

!DriveSystem methodsFor: 'target speed' stamp: 'FHS 7/6/2023 20:19:25'!
targetSpeed
	^targetSpeed! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DriveSystem class' category: 'ISW1-2023-1C-2doParcial'!
DriveSystem class
	instanceVariableNames: ''!

!DriveSystem class methodsFor: 'instance creation' stamp: 'FHS 7/6/2023 20:21:44'!
withTargetSpeed: aTargetSpeed
	^self new initializeWithTargetSpeed: aTargetSpeed ! !


!classDefinition: #DrivingAssistant category: 'ISW1-2023-1C-2doParcial'!
Object subclass: #DrivingAssistant
	instanceVariableNames: 'speed speedSensor frontProximitySensor distanceToCurrentObject driveSystem'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!DrivingAssistant methodsFor: 'initialization' stamp: 'FHS 7/6/2023 20:22:55'!
startWithDrivingModeAutomaticWithSpeedSensor: aSpeedSensor withProximitySensor: aProximitySensor withDriveSystem: aDriveSystem    
	speed := 0*(kilometer / hour).
	speedSensor := aSpeedSensor.
	frontProximitySensor := aProximitySensor.
	driveSystem := aDriveSystem.
	! !


!DrivingAssistant methodsFor: 'accessing-private' stamp: 'FHS 7/6/2023 19:01:12'!
distanceToCurrentObject
	^distanceToCurrentObject! !


!DrivingAssistant methodsFor: 'throttle' stamp: 'FHS 7/6/2023 19:57:56'!
connectThrottle
	driveSystem connectThrottle.! !

!DrivingAssistant methodsFor: 'throttle' stamp: 'FHS 7/6/2023 19:56:42'!
disconnectThrottle
	driveSystem disconnectThrottle.! !


!DrivingAssistant methodsFor: 'tick' stamp: 'FHS 7/6/2023 21:34:48'!
tick
	self subclassResponsibility ! !


!DrivingAssistant methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:50:52'!
dangerProximityBeep
	driveSystem dangerProximityBeep! !

!DrivingAssistant methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:53:53'!
turnOffProximityBeep
	driveSystem turnOffProximityBeep.! !

!DrivingAssistant methodsFor: 'beep' stamp: 'FHS 7/6/2023 19:52:35'!
warningProximityBeep
	driveSystem warningProximityBeep.! !


!DrivingAssistant methodsFor: 'brakes' stamp: 'FHS 7/6/2023 20:01:36'!
applyBrakes
	driveSystem applyBrakes.! !

!DrivingAssistant methodsFor: 'brakes' stamp: 'FHS 7/6/2023 20:04:17'!
releaseBrakes
	driveSystem releaseBrakes.! !


!DrivingAssistant methodsFor: 'speed' stamp: 'FHS 7/6/2023 20:24:27'!
speed
	^speed! !


!DrivingAssistant methodsFor: 'target speed' stamp: 'FHS 7/6/2023 20:27:36'!
keepSpeedAt: aTargetSpeed
	driveSystem keepSpeedAt: aTargetSpeed.! !


!DrivingAssistant methodsFor: 'dmf' stamp: 'FHS 7/6/2023 20:33:12'!
actualDMF
	^(speed)^2 / (180000*kilometer/(hour^2)).! !

!DrivingAssistant methodsFor: 'dmf' stamp: 'FHS 7/6/2023 21:24:37'!
calculateZoneDistance
	|dmf|
	dmf := self actualDMF.
	
	(distanceToCurrentObject isNil or: [dmf*2 < distanceToCurrentObject]) ifTrue: [
			^SafeZone new.
		].
	(0*meter <= distanceToCurrentObject and: [distanceToCurrentObject <= dmf]) ifTrue: [
			^DangerZone new.
		].
	(dmf <= distanceToCurrentObject and: [distanceToCurrentObject <= (dmf*2)]) ifTrue: [
			^WarningZone new.
		].
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DrivingAssistant class' category: 'ISW1-2023-1C-2doParcial'!
DrivingAssistant class
	instanceVariableNames: ''!

!DrivingAssistant class methodsFor: 'instance creation' stamp: 'FHS 7/6/2023 21:33:37'!
startWithDrivingModeAutomaticWithSpeedSensor: aSpeedSensor 
	withProximitySensor: aProximitySensor 
	withDriveSystem: aDriveSystem 
	
	^Automatic new startWithDrivingModeAutomaticWithSpeedSensor: aSpeedSensor withProximitySensor: aProximitySensor withDriveSystem: aDriveSystem ! !

!DrivingAssistant class methodsFor: 'instance creation' stamp: 'FHS 7/6/2023 21:43:52'!
startWithDrivingModeManualAssistedWithSpeedSensor: aSpeedSensor 
	withProximitySensor: aProximitySensor 
	withDriveSystem: aDriveSystem 
	
	^ManualAssisted new startWithDrivingModeAutomaticWithSpeedSensor: aSpeedSensor withProximitySensor: aProximitySensor withDriveSystem: aDriveSystem ! !

!DrivingAssistant class methodsFor: 'instance creation' stamp: 'FHS 7/6/2023 21:33:06'!
startWithDrivingModeManualWithSpeedSensor: aSpeedSensor 
	withProximitySensor: aProximitySensor 
	withDriveSystem: aDriveSystem  
	
	^Manual new startWithDrivingModeAutomaticWithSpeedSensor: aSpeedSensor withProximitySensor: aProximitySensor withDriveSystem: aDriveSystem.! !


!classDefinition: #Automatic category: 'ISW1-2023-1C-2doParcial'!
DrivingAssistant subclass: #Automatic
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!Automatic methodsFor: 'tick' stamp: 'FHS 7/6/2023 21:34:32'!
tick
	| zoneDistance |
	speed := speedSensor tick.
	distanceToCurrentObject := frontProximitySensor tick.
	
	zoneDistance := self calculateZoneDistance.
	
	zoneDistance automaticBehaviorWith: self.! !


!Automatic methodsFor: 'target speed' stamp: 'FHS 7/6/2023 21:41:32'!
reduceTargetSpeedBy: aSpeed
	driveSystem keepSpeedAt: (driveSystem targetSpeed - aSpeed ).! !


!classDefinition: #Manual category: 'ISW1-2023-1C-2doParcial'!
DrivingAssistant subclass: #Manual
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!Manual methodsFor: 'tick' stamp: 'FHS 7/6/2023 21:34:18'!
tick
	| zoneDistance |
	speed := speedSensor tick.
	distanceToCurrentObject := frontProximitySensor tick.
	
	zoneDistance := self calculateZoneDistance.
	
	zoneDistance manualBehaviorWith: self.! !


!classDefinition: #ManualAssisted category: 'ISW1-2023-1C-2doParcial'!
DrivingAssistant subclass: #ManualAssisted
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!ManualAssisted methodsFor: 'tick' stamp: 'FHS 7/6/2023 21:44:45'!
tick
	| zoneDistance |
	speed := speedSensor tick.
	distanceToCurrentObject := frontProximitySensor tick.
	
	zoneDistance := self calculateZoneDistance.
	
	zoneDistance manualBehaviorWith: self.
	zoneDistance automaticBehaviorWith: self.! !


!ManualAssisted methodsFor: 'target speed' stamp: 'FHS 7/6/2023 21:45:22'!
reduceTargetSpeedBy: aSpeed
	driveSystem keepSpeedAt: (driveSystem targetSpeed - aSpeed ).! !


!classDefinition: #ProximitySensor category: 'ISW1-2023-1C-2doParcial'!
Object subclass: #ProximitySensor
	instanceVariableNames: 'events'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!ProximitySensor methodsFor: 'tick' stamp: 'FHS 7/6/2023 19:01:44'!
tick
	^events next! !


!ProximitySensor methodsFor: 'initialization' stamp: 'FHS 7/6/2023 19:17:19'!
initializeWithEvents: anStreamOfEvents 
	events := anStreamOfEvents.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ProximitySensor class' category: 'ISW1-2023-1C-2doParcial'!
ProximitySensor class
	instanceVariableNames: ''!

!ProximitySensor class methodsFor: 'instance creation' stamp: 'FHS 7/6/2023 19:17:11'!
withEvents: anStreamOfEvents 
	^self new initializeWithEvents: anStreamOfEvents ! !


!classDefinition: #SpeedSensor category: 'ISW1-2023-1C-2doParcial'!
Object subclass: #SpeedSensor
	instanceVariableNames: 'events'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2023-1C-2doParcial'!

!SpeedSensor methodsFor: 'initialize' stamp: 'FHS 7/6/2023 19:16:26'!
initializeWithEvents: anStreamOfEvents 
	events := anStreamOfEvents.! !


!SpeedSensor methodsFor: 'tick' stamp: 'FHS 7/6/2023 18:31:15'!
tick
	^events next! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SpeedSensor class' category: 'ISW1-2023-1C-2doParcial'!
SpeedSensor class
	instanceVariableNames: ''!

!SpeedSensor class methodsFor: 'instance creation' stamp: 'FHS 7/6/2023 19:16:35'!
withEvents: anStreamOfEvents 
	^self new initializeWithEvents: anStreamOfEvents ! !
